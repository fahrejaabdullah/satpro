
import swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'

const alert = {
    showAlert(message, status) {
      swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: message,
        confirmButtonText: 'Reload',
        allowOutsideClick: true,
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(result => {
        if (status === 401) {
          sessionStorage.clear()
          localStorage.clear()
          window.location.href = '/'
        }
        else if (result.value) {
          window.location.reload();
        }
      });
    },
    alertCreateEditInfo(message, status) {
      swal.fire({
        icon: 'info',
        title: `Info`,
        text: message,
        confirmButtonText: 'OK',
        allowOutsideClick: true,
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(result => {
        if (status === 401) {
          sessionStorage.clear()
          localStorage.clear()
          window.location.href = '/'
        }
        else if (result.value) {
          window.location.reload();
        }
      });
    },
    alertCreateEditSuccess(message, status) {
      swal.fire({
        icon: 'success',
        title: `Success`,
        text: message,
        confirmButtonText: 'OK',
        allowOutsideClick: true,
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(result => {
        if (status === 401) {
          sessionStorage.clear()
          localStorage.clear()
          window.location.href = '/'
        }
        else if (result.value) {
          window.location.reload();
        }
      });
    },
    alertCreateEditError(message, from, type, status) {
      swal.fire({
        icon: 'error',
        title: `${type} ${from} Failed`,
        text: message,
        cancelButtonText: 'Close',
        allowOutsideClick: true,
        showConfirmButton: false,
        showCancelButton: true,
        cancelButtonColor: '#f27474',
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(result => {
        if (status === 401) {
          sessionStorage.clear()
          localStorage.clear()
          window.location.href = '/'
        }
        else if (result.value) {
          window.location.reload();
        }
      });
    },
    alertCreateEditErrorWithReload(message, status) {
      swal.fire({
        icon: 'success',
        title: `Success`,
        text: message,
        confirmButtonText: 'OK',
        allowOutsideClick: false,
        showClass: {
          popup: 'animate__animated animate__fadeInDown'
        },
        hideClass: {
          popup: 'animate__animated animate__fadeOutUp'
        }
      }).then(result => {
        if (status === 401) {
          sessionStorage.clear()
          localStorage.clear()
          window.location.href = '/'
        }
        else if (result.value) {
          window.location.reload();
        }
      });
    },

  };

export { alert }