import React, {Component, useEffect} from 'react';
import { Route, Redirect, HashRouter, Switch, useLocation } from 'react-router-dom';
import { Layout, notification } from 'antd';
import SideBarComponent from './components/SideBar';
import Login from './views/Login';
import HeaderComponent from './components/Header';
import ForgetPassword from "./views/ForgetPassword";
import KatalogAcara from './views/KatalogAcara/list';
import KatalogDetail from './views/KatalogAcara/detail'
import KatalogCreate from './views/KatalogAcara/create'
import KatalogAcaraUser from './views/KatalogAcaraUser/list';
import KatalogAcaraUserDetail from './views/KatalogAcaraUser/detail';
import DoorprizeUser from './views/DoorprizeUser/list';
import AcaraSaya from './views/AcaraSaya/list';
import AcaraSayaDetail from './views/AcaraSaya/detail';
import Pembayaran from './views/Pembayaran/list'
import NotFound from './views/NotFound';
import Dashboard from './views/Dashboard';
import Register from './views/Register';
import Welcome from './views/Welcome';
import Verifikasi from './views/Verifikasi';
import ChangePassword from './views/ChangePassword';
import Doorprize from './views/Doorprize/list';
import DoorprizeCreate from './views/Doorprize/create';
import User from './views/User/list';
import UserCreate from './views/User/create';
import UpdateProfile from './views/UpdateProfile/form';


function PrivateRoute ({ ...props }) {
  if (
    sessionStorage.getItem('token') &&
    sessionStorage.getItem('name') &&
    sessionStorage.getItem("userId") &&
    sessionStorage.getItem("userData")
  ) {
    return <Route { ...props } />
  } else {
    sessionStorage.clear();
    return <Redirect to="/" />
  }
}


function DashboardRoute (props) {
  const name = sessionStorage.getItem('name') ? sessionStorage.getItem('name') : localStorage.getItem('name') ? localStorage.getItem('name') : null;
  const isLogin = sessionStorage.getItem('token') ? sessionStorage.getItem('token') : localStorage.getItem('token') ? localStorage.getItem('token') : null;
  if (isLogin) {
    return (
      <Layout>
        <SideBarComponent {...props} name={name}/>
        <Layout>
          <HeaderComponent {...props} name={name}/>
          <Layout.Content>
            <Route { ...props } />
          </Layout.Content>
        </Layout>
      </Layout>
    )
  } else {
    sessionStorage.clear();
    notification.info({
      message: 'Info',
      description: 'Harap login untuk mengakses EL QUR’AN TV!'
    })
    return <Redirect to="/" />
  }
}

function FreeRoute (props) {
  const name = sessionStorage.getItem('name') ? sessionStorage.getItem('name') : localStorage.getItem('name') ? localStorage.getItem('name') : null;
  if(props.location.pathname === "/katalog-acara") {
    return (
      <Layout>
        <SideBarComponent {...props} name={name}/>
        <Layout>
          <HeaderComponent {...props} name={name}/>
          <Layout.Content>
            <Route { ...props } />
          </Layout.Content>
        </Layout>
      </Layout>
    )
  }else {
    sessionStorage.clear();
    notification.info({
      message: 'Info',
      description: 'Harap login untuk mengakses EL QUR’AN TV!'
    })
    return <Redirect to="/" />
  }
}

function OpenRoute (props) {
  const isLogin = sessionStorage.getItem('token') ? sessionStorage.getItem('token') : localStorage.getItem('token') ? localStorage.getItem('token') : null;
  if (isLogin) {
    const currentRole = sessionStorage.getItem('userRole') ? sessionStorage.getItem('userRole') : localStorage.getItem('userRole') ? localStorage.getItem('userRole') : "Free Role";
    if(currentRole === 'User' && (props.location.pathname === "/" || props.location.pathname.includes("/verifikasi/"))){
      return (
        <Route { ...props } />
      )
    }else{
      const redirection = {
        'Admin': '/dashboard',
        'Free Role': '/',
        'User': '/'
      }
      return (<Redirect to={redirection[currentRole]} />)

    }
  } else {
    sessionStorage.setItem('userRole', 'Free Role')
    return <Route { ...props } />
  }
}

export function RouteWrapper(props) {
  const location = useLocation();

  useEffect(() => {
    props.onRouteUpdate();
  },[location.pathname]);

  return (props.children)
}


export default class Routes extends Component {
  constructor(props) {
    super(props)
    this.state = {
      token: sessionStorage.getItem('token') ? sessionStorage.getItem('token') : localStorage.getItem('token') ? localStorage.getItem('token') : null,
      name: sessionStorage.getItem('name') ? sessionStorage.getItem('name') : localStorage.getItem('name') ? localStorage.getItem('name') : null,
      userId: sessionStorage.getItem('userId') ? sessionStorage.getItem('userId') : localStorage.getItem('userId') ? localStorage.getItem('userId') : null,
      userData: sessionStorage.getItem('userData') ? sessionStorage.getItem('userData') : localStorage.getItem('userData') ? localStorage.getItem('userData') : null,
    }
  }

  updateContext = () => {
    var token;
    var name;
    var userId;
    var userData;
    if(localStorage.getItem('token')){
      token = localStorage.getItem('token');
      name = localStorage.getItem('name');
      userId = localStorage.getItem('userId') ;
      userData = localStorage.getItem('userData');
    }else{
      token = sessionStorage.getItem('token');
      name = sessionStorage.getItem('name');
      userId = sessionStorage.getItem('userId');
      userData = sessionStorage.getItem('userData');
    }
    this.setState({token, name, userId, userData});
  }

  render() {
    const context = { ...this.state };

    return(
      <HashRouter>
        <RouteWrapper onRouteUpdate={this.updateContext}>
          <Switch>
            <OpenRoute exact path="/" render={props => <Welcome isMobile={this.props.isMobile} {...props} {...context}/>} />
            <OpenRoute exact path="/Login" render={props => <Login onLogin={() => this.updateContext()} isMobile={this.props.isMobile}  {...props}/>} />
            <OpenRoute exact path="/ForgetPassword" render={ props => <ForgetPassword isMobile={this.props.isMobile}/>}  />
            <OpenRoute exact path="/register" render={ props => <Register isMobile={this.props.isMobile}/>}  />
            <OpenRoute exact path="/verifikasi/:email" render={ props => <Verifikasi isMobile={this.props.isMobile}/>}  />
            <OpenRoute exact path="/change-password/:email" render={ props => <ChangePassword isMobile={this.props.isMobile}/>}  />
            {/* Dashboard */}
            {/* <DashboardRoute exact path="/" render={props => <Welcome  {...props}/>} /> */}
            <DashboardRoute exact path="/dashboard" isMobile={this.props.isMobile} render={ props => <Dashboard {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Katalog Acara User */}
            <FreeRoute exact path="/katalog-acara" isMobile={this.props.isMobile} render={ props => <KatalogAcaraUser {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/katalog-acara/:id" isMobile={this.props.isMobile} render={ props => <KatalogAcaraUserDetail {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Door Prize User */}
            <DashboardRoute exact path="/doorprize" isMobile={this.props.isMobile} render={ props => <DoorprizeUser {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Acara Saya */}
            <DashboardRoute exact path="/acara-saya" isMobile={this.props.isMobile} render={ props => <AcaraSaya {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/acara-saya/:id" isMobile={this.props.isMobile} render={ props => <AcaraSayaDetail {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Katalog Acara */}
            <DashboardRoute exact path="/katalog-acara-admin" isMobile={this.props.isMobile} render={ props => <KatalogAcara {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/Katalog-detail-admin/:id" isMobile={this.props.isMobile} render={ props => <KatalogDetail {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/Katalog-add" isMobile={this.props.isMobile} render={ props => <KatalogCreate {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/Katalog-edit/:id" isMobile={this.props.isMobile} render={ props => <KatalogCreate {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Hadiah / Doorprize */}
            <DashboardRoute exact path="/hadiah-doorprize" isMobile={this.props.isMobile} render={ props => <Doorprize {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/doorprize-add" isMobile={this.props.isMobile}  render={ props => <DoorprizeCreate {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/doorprize-edit/:id" isMobile={this.props.isMobile} render={ props => <DoorprizeCreate {...props} isMobile={this.props.isMobile} {...context} />}  />
            
            {/* Master User */}
            <DashboardRoute exact path="/user" isMobile={this.props.isMobile} render={ props => <User {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/user-add" isMobile={this.props.isMobile} render={ props => <UserCreate {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/user-edit/:email" isMobile={this.props.isMobile} render={ props => <UserCreate {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Update Profile */}
            <DashboardRoute exact path="/update-profile-admin/:email" isMobile={this.props.isMobile} render={ props => <UpdateProfile {...props} isMobile={this.props.isMobile} {...context} />}  />
            <DashboardRoute exact path="/update-profile" isMobile={this.props.isMobile} render={ props => <UpdateProfile {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Pembayaran */}
            <DashboardRoute exact path="/pembayaran" isMobile={this.props.isMobile} render={ props => <Pembayaran {...props} isMobile={this.props.isMobile} {...context} />}  />

            {/* Admin Keuangan */}
            <Route path="*" component={NotFound} />
          </Switch>
        </RouteWrapper>
      </HashRouter>
    )
  }
}
