import { get, put, post } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config

const normalizeDataLov = res => {
  let dataTemp = res.data.data
  let arrayData = [];
  dataTemp.map((response, index) => {
    arrayData.push({
      "value":response.id_acara,
      "label":response.nama,
      "id":response.id_acara,
      "key":response.nama + response.id_acara
    })
  }
  );
  return arrayData;
}

const normalizeDataUser = res => {
  let dataTemp = res.data.data
  let arrayData = [];
  dataTemp.map((response, index) => {
    arrayData.push({
      "value":response.email,
      "label":response.nama,
      "id":response.email,
      "key":response.nama + index
    })
  }
  );
  return arrayData;
}


const services = {
  getAcara(token){
    let url = `${BASE_URL}rest/admin/secured/acara/ol/acara?search=&size=999&page=0`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return normalizeDataLov(response)
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  getDataDetail(param){
    let url = `${BASE_URL}rest/admin/secured/doorprize/find/${param.id}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  getPemenang(token,id){
    let url = `${BASE_URL}rest/admin/secured/user/ol/user?idAcara=`+id+`&search=`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){

        return normalizeDataUser(response)
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  getHadiah(token){
    let url = `${BASE_URL}rest/admin/secured/doorprize/ol/jenis-hadiah`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response.data
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  getStatus(token){
    let url = `${BASE_URL}rest/admin/secured/doorprize/ol/status`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response.data
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  create(token, body) {
    return post(`${BASE_URL}rest/admin/secured/doorprize/save`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess(response.data.info.message, response.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(response.data.info.message, 'Data', 'Create', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Create', err.response.data.status)
      return 'error'
    })
  },
  edit(token, id, body) {
    return put(`${BASE_URL}rest/admin/secured/doorprize/update/${id}`,
      body,
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.status === "success"){
        alert.alertCreateEditSuccess(res.data.info.message, res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res.data.info.message, 'Data', 'Edit', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Edit', err.response.data.status)
      return 'error'
    })
  },

  deleteData(token, id) {
    return put(`${BASE_URL}rest/admin/secured/doorprize/delete/${id}`,
      {},
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess('Delete Hadiah / Doorprize Berhasil', response.data.status)
        return response.data.status
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Delete', err.response.data.status)
      return 'error'
    })
  },
  exportData(token) {
    let url = `${BASE_URL}rest/admin/secured/doorprize/export-report`
    return get(url, {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
        return response
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
}
export { services }
