import { get, put, post } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config

const services = {
  getDataDetail(param){
    let url = `${BASE_URL}rest/admin/secured/acara/find/${param.id}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  getJenisAcara(token){
    let url = `${BASE_URL}rest/admin/secured/acara/ol/jenis-acara`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response.data
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  create(token, body) {
    return post(`${BASE_URL}rest/admin/secured/acara/save`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess(response?.data?.info?.message || 'success', response.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(response?.data?.info?.message || 'error', 'Data', 'Create', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Create', err.response.data.status)
      return 'error'
    })
  },
  edit(token, id, body) {
    return put(`${BASE_URL}rest/admin/secured/acara/update/${id}`,
      body,
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.status === "success"){
        alert.alertCreateEditSuccess(res?.data?.info?.message || 'success', res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res?.data?.info?.message || 'error', 'Data', 'Edit', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Edit', err.response.data.status)
      return 'error'
    })
  },
  deleteData(token, id) {
    return put(`${BASE_URL}rest/admin/secured/acara/delete/${id}`,
      {},
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess('Delete Acara Berhasil', response.data.status)
        return response.data.status
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.message || 'error', 'Data', 'Delete', err.response.data.status)
      return 'error'
    })
  },
  exportData(token) {
    let url = `${BASE_URL}rest/admin/secured/acara/export-report`
    return get(url, {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
        return response
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  exportDataPerserta(token, id) {
    let url = `${BASE_URL}rest/admin/secured/acara/find-partisipasi/export-report?idAcara=${id}`
    return get(url, {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
        return response
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  }
}
export { services }
