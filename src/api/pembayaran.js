import { get } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config

const services = {
  exportData(token) {
    let url = `${BASE_URL}rest/admin/secured/pembayaran/export-report`
    return get(url, {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
        return response
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
}
export { services }
