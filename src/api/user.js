import { get, put, post } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config

const services = {
  exportData(token) {
    let url = `${BASE_URL}rest/admin/secured/user/export-report`
    return get(url, {
      responseType: 'blob',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
        return response
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  getDataDetail(param){
    let url = `${BASE_URL}rest/admin/secured/user/find/${param.id}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  getDataUser(param){
    let url = `${BASE_URL}rest/web/secured/profile/my-profile`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(resGet => {
      if(resGet.data.status === "success"){
        return resGet
      }else{
        alert.showAlert(resGet.data.info.message, resGet.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },

  verifikasi(token, body) {
    return post(`${BASE_URL}rest/web/open/auth/kirim-verikasi`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess(response.data.info.message, response.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(response.data.info.message, 'Data', 'Create', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Create', err.response.data.status)
      return 'error'
    })
  },

  create(token, body) {
    return post(`${BASE_URL}rest/admin/secured/user/save`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: TIMEOUT
      }
    ).then(resCreate => {
      if(resCreate.data.status === "success"){
        alert.alertCreateEditSuccess(resCreate.data.info.message, resCreate.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(resCreate.data.info.message, 'Data', 'Create', resCreate.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Create', err.response.data.status)
      return 'error'
    })
  },
  edit(token, id, body) {
    return put(`${BASE_URL}rest/admin/secured/user/update/${id}`,
      body,
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.status === "success"){
        alert.alertCreateEditSuccess(res.data.info.message, res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res.data.info.message, 'Data', 'Edit', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Edit', err.response.data.status)
      return 'error'
    })
  },
  editUser(token, body) {
    return put(`${BASE_URL}rest/web/secured/profile/update-profile`,
      body,
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(resEdit => {
      if(resEdit.data.status === "success"){
        alert.alertCreateEditSuccess(resEdit.data.info.message, resEdit.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(resEdit.data.info.message, 'Data', 'Edit', resEdit.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Edit', err.response.data.status)
      return 'error'
    })
  },
  deleteData(token, id) {
    return put(`${BASE_URL}rest/admin/secured/user/delete/${id}`,
      {},
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess('Delete Acara Berhasil', response.data.status)
        return response.data.status
      }else{
        alert.showAlert(response.data.info.message, response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Delete', err.response.data.status)
      return 'error'
    })
  },

}
export { services }
