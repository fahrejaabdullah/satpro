import { get, put } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config


const getChartAcaraNow = (token, param) => {
  return get(`${BASE_URL}rest/admin/secured/dashboard/grafik?year=${param.year}&month=${param.month}`,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }
  ).then(response => {
    if(response.data.status === "success"){
      return response?.data?.data || []
    }else{
      alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
      return 'error'
    }
  }).catch(err => {
    alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
    return {}
  })
}

const getChartAcaraAll = (token, key, body) => {
  return put(`${BASE_URL}be/api/admin/master/projectStatus/update?key=${key}`, body,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }
  ).then(response => {
    if(response.data.status === "success"){
      return response?.data?.data || []
    }else{
      alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
      return 'error'
    }
  }).catch(err => {
    alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
    return {}
  })
}




export default {
  getChartAcaraNow,
  getChartAcaraAll,
}
