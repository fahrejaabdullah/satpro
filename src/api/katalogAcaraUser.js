import { get, put, post } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config

const endpoint = {
  getAcara(param){
    let url = `${BASE_URL}rest/web/open/acara/find-all?search=${param.search}&size=${param.size}&page=${param.page - 1}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response.data.data
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  getAcaraDetail(param){
    let url = `${BASE_URL}rest/web/secured/acara/find/${param.id}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response.data
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  regisAcara(param) {
    return post(`${BASE_URL}rest/web/secured/acara/daftar-acara/${param.id}`,
      {},
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${param.token}`,
        },
        timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditErrorWithReload(response?.data?.info?.message || 'success', response.data.status)
        return response?.data?.data
      }else{
        alert.alertCreateEditError(response?.data?.info?.message || 'error', 'Data', 'Create', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err?.response?.data?.info?.message || 'error', 'Data', 'Create', err.response.data.status)
      return 'error'
    })
  },
}
export { endpoint }
