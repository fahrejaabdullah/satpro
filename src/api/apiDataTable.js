import { get } from 'axios'
import config from '../services/config'
import qs from 'querystring'
import { alert } from '../functions/alert'
import moment from 'moment';
const { BASE_URL, TIMEOUT } = config

const normalizeQuery = (url) => {
  return qs.stringify(url)
}

const normalizeData = res => {
  let dataTemp = res.data
  const normalize = res.data.data.content.map(dataObj => {
    return {
      idAcara: dataObj.idAcara,
      nama: dataObj.nama,
      deskripsi: dataObj.deskripsi,
      media: dataObj.media,
      tanggal: dataObj.tanggal,
      id: dataObj.idAcara,
      narasumber: dataObj.narasumber
    }
  })
  return {
    data: normalize,
    totalData: dataTemp?.data?.totalElements || 0,
    status: 'success',
  }
}


        
        
        

const normalizeDataUser = res => {
  let dataTemp = res.data
  const normalize = res.data.data.content.map(dataObj => {
    return {
      email:dataObj.email,
      nama:dataObj.nama,
      noTelp:dataObj.noTelp,
      tanggalLahir: dataObj.tanggalLahir ? moment(dataObj.tanggalLahir).format("dddd,DD-MM-YYYY") : "-",
    }
  })
  return {
    data: normalize,
    totalData: dataTemp?.data?.totalElements || 0,
    status: 'success',
  }
}

const normalizeDataDoorprize = res => {
  let dataTemp = res.data
  const normalize = res.data.data.content.map(dataObj => {
    return {
      idDoorprize : dataObj.idDoorprize,
      namaAcara: dataObj.namaAcara,
      namaPemenang : dataObj.namaPemenang,
      noResi: dataObj.noResi,
      jenis: dataObj.jenis,
      status: dataObj.status,
    }
  })
  return {
    data: normalize,
    totalData: dataTemp?.data?.totalElements || 0,
    status: 'success',
  }
}

const normalizeDataPembayaran = res => {
  let dataTemp = res.data
  const normalize = res.data.data.content.map(dataObj => {
    return {
      idPembayaran: dataObj.idPembayaran,
      nama: dataObj.nama,
      email: dataObj.email,
      noTelp: dataObj.noTelp,
      tanggal: dataObj.tanggal,
      id: dataObj.idPembayaran,
      namaAcara: dataObj.namaAcara,
      nominal: dataObj.nominal,
      pembayaranStatus: dataObj.pembayaranStatus
    }
  })
  return {
    data: normalize,
    totalData: dataTemp?.data?.totalElements || 0,
    status: 'success',
  }
}

const normalizeDataPartisipan = res => {
  let dataTemp = res.data
  const normalize = res.data.data.content.user.map((dataObj, index) => {
    return {
      nama: dataObj.nama,
      email: dataObj.email,
      noTelp: dataObj.noTelp,
      tanggalLahir: dataObj.tanggalLahir,
      id: dataObj.nama + index,
    }
  })
  return {
    data: normalize,
    totalData: dataTemp?.data?.totalElements || 0,
    status: 'success',
  }
}

const handleArr = err => {
  alert.showAlert(err.response.info, err.response.status)
    return {
      data: [],
      totalData: 0,
      status: 'error',
    }
}

const normalizeDataLov = res => {
  let dataTemp = res.data
  return {
    data: dataTemp.data,
    status: 'success',
    rc: dataTemp.rc
  }
}


const endpoint = {
  getDataLov(param){
    const urlString = normalizeQuery({
      keyword: param,
      isEng: 0,
      limit: 5
    })
    return get(`${BASE_URL}noAuth/be/api/lov/admin/galleryEvent?${urlString}` , {
      headers: {
        'Content-Type': 'application/json',
        // Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.rc === "00"){
        return normalizeDataLov(res)
      }else{
        return 'error'
      }
    }).catch(err => {
      return {
        data: [],
        totalData: 0,
        status: 'error',
      }
    })
  }, 

  // API get catalog admin
  getDataListCatalog(param){
    const urlString = normalizeQuery({
      sort: param.sort,
      size: param.size,
      page: param.page - 1,
      search: param.search
    })
    return get(`${BASE_URL}rest/admin/secured/acara/find-all?${urlString}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.status === "success"){
        return normalizeData(res)
      }else{
        alert.showAlert(res?.data?.message || 'error', res.data.status)
        return 'error'
      }
    }).catch(err => {
      handleArr(err)
    })
  },

  // data peserta
  getDataPeserta(param){
    const urlString = normalizeQuery({
      sort: param.sort,
      size: param.size,
      page: param.page - 1,
      search: param.search,
      idAcara: param.idAcara
    })
    return get(`${BASE_URL}rest/admin/secured/acara/find-partisipasi?${urlString}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.status === "success"){
        return normalizeDataPartisipan(res)
      }else{
        alert.showAlert(res?.data?.message || 'error', res.data.status)
        return 'error'
      }
    }).catch(err => {
      handleArr(err)
    })
  },

  //API get doorprize admin
  getDataListDoorprize(param){
    const urlString = normalizeQuery({
      sort: param.sort,
      size: param.size,
      page: param.page - 1,
      search: param.search
    })
    return get(`${BASE_URL}rest/admin/secured/doorprize/find-all?${urlString}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.status == "success"){
        return normalizeDataDoorprize(res)
      }else{
        alert.showAlert(res?.data?.message || 'error', res.data.status)
        return 'error'
      }
    }).catch(err => {
      handleArr(err)
    })
  },

  // API get user admin
  getDataListUser(param){
    const urlString = normalizeQuery({
      sort: param.sort,
      size: param.size,
      page: param.page - 1,
      search: param.search
    })
    return get(`${BASE_URL}rest/admin/secured/user/find-all?${urlString}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.status === "success"){
        return normalizeDataUser(res)
      }else{
        alert.showAlert(res?.data?.message || 'error', res.data.status)
        return 'error'
      }
    }).catch(err => {
      handleArr(err)
    })
  },

  // get data pembayaran
  getDataListPembayaran(param){
    const urlString = normalizeQuery({
      sort: param.sort,
      size: param.size,
      page: param.page - 1,
      search: param.search
    })
    let url = `${BASE_URL}rest/admin/secured/pembayaran/find-all?${urlString}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.status === "success"){
        return normalizeDataPembayaran(res)
      }else{
        alert.showAlert(res?.data?.message || 'error', res.data.status)
        return 'error'
      }
    }).catch(err => {
      handleArr(err)
    })
  },

}
export { endpoint }
