import { get, put } from 'axios'
import config from '../services/config'
import { alert } from '../functions/alert'
const { BASE_URL, TIMEOUT } = config

const endpoint = {
  getAcara(param){
    let url = `${BASE_URL}rest/web/secured/acara/acara-saya?search=${param.search}&size=${param.size}&page=${param.page - 1}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response?.data?.data || []
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  getAcaraDetail(param){
    let url = `${BASE_URL}rest/web/secured/acara/find/${param.id}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      if(response.data.status === "success"){
        return response.data
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err?.response?.data?.info?.message || 'error', err.response.data.status)
      return {}
    })
  },
  
  deleteData(token, id) {
    return put(`${BASE_URL}rest/web/secured/acara/batal-acara/${id}`,
      {},
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(response => {
      if(response.data.status === "success"){
        alert.alertCreateEditSuccess('Delete Acara Berhasil', response.data.status)
        return response.data.status
      }else{
        alert.showAlert(response?.data?.info?.message || 'error', response.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Delete', err.response.data.status)
      return 'error'
    })
  },
}
export { endpoint }
