import { get, put, post } from 'axios'
import config from '../services/config'
import qs from 'querystring'
import { alert } from '../functions/alert'
const { BASE_URL_LOGIN, TIMEOUT, BASE_URL_NOAUTH } = config

const normalizeQuery = (url) => {
  return qs.stringify(url)
}

const normalizeData = res => {
  let dataTemp = res.data
  const normalize = res.data.data.list.map(dataObj => {
    return {
      name: dataObj.name,
      day: dataObj.day,
      date: dataObj.date,
      time: dataObj.time,
      media: dataObj.media,
      id: dataObj.eventId,
      eventId: dataObj.eventId
    }
  })
  return {
    data: normalize,
    totalData: dataTemp.data.totalItem,
    status: 'success',
    rc: dataTemp.rc
  }
}
const normalizeDataLov = res => {
  let dataTemp = res.data
  return {
    data: dataTemp.data,
    status: 'success',
    rc: dataTemp.rc
  }
}


const endpoint = {
  getDataLov(param){
    const urlString = normalizeQuery({
      keyword: param,
      isEng: 0,
      limit: 5
    })
    return get(`${BASE_URL_LOGIN}noAuth/be/api/lov/admin/galleryEvent?${urlString}` , {
      headers: {
        'Content-Type': 'application/json',
        // Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.rc == "00"){
        return normalizeDataLov(res)
      }else{
        return 'error'
      }
    }).catch(err => {
      return {
        data: [],
        totalData: 0,
        status: 'error',
      }
    })
  },
  uploadBanner(token, body, url){
    return post(`${BASE_URL_LOGIN}be/api/yayasanLoyola/upload/${url} `,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: 60000
      }
    ).then(res => {
      return res.data
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Image', 'Upload', err.response.status)
      return 'error'
    })
  },
  getData(param){
    const urlString = normalizeQuery({
      sort: param.sort,
      size: param.size,
      page: param.page - 1,
      keyword: param.search,
      isEng: 0
    })
    return get(`${BASE_URL_LOGIN}be/api/admin/gallery/load?${urlString}`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.rc == "00"){
        return normalizeData(res)
      }else{
        alert.showAlert(res.data.message, res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err.response.data.message, err.response.status)
      return {
        data: [],
        totalData: 0,
        status: 'error',
      }
    })
  },
  getDataDetail(param){
    const urlString = normalizeQuery({
      eventId: param.id,
    })
    let url = `${BASE_URL_LOGIN}be/api/admin/gallery/detail?${urlString}`
    return get(url, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${param.token}`,
      },
      timeout: TIMEOUT
    }).then(response => {
      return response
    }).catch(err => {
      alert.showAlert(err.response.data.message, err.response.status)
      return {}
    })
  },
  getExport(token){
    return post(`${BASE_URL_LOGIN}be/api/admin/gallery/export`, {},{
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      timeout: TIMEOUT
    }).then(res => {
      if(res.data.rc == "00"){
        // alert.alertCreateEditSuccess(res.data.message)
        return res.data.data.link
      }else{
        alert.alertCreateEditError(res.data.message, 'Data', 'Create', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.showAlert(err.response.data.message, err.response.status)
      return 'error'
    })
  },
  create(token, body) {
    return post(`${BASE_URL_LOGIN}be/api/admin/gallery/insert`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.rc == "00"){
        alert.alertCreateEditSuccess(res.data.message, res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res.data.message, 'Data', 'Create', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Create', err.response.status)
      return 'error'
    })
  },
  createbyId(token, body) {
    return post(`${BASE_URL_LOGIN}be/api/admin/gallery/insertOnlyGallery`,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.rc == "00"){
        alert.alertCreateEditSuccess(res.data.message, res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res.data.message, 'Data', 'Create', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Create', err.response.status)
      return 'error'
    })
  },
  edit(token, id, body) {
    const urlString = normalizeQuery({
      // eventId: id,
    })
    return put(`${BASE_URL_LOGIN}be/api/admin/gallery/updateGallery?${urlString}`,
      body,
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.rc == "00"){
        alert.alertCreateEditSuccess(res.data.message, res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res.data.message, 'Data', 'Edit', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Edit', err.response.status)
      return 'error'
    })
  },
  deleteData(token, userId, id) {
    const urlString = normalizeQuery({
      eventId: id,
      userId: userId
    })
    return put(`${BASE_URL_LOGIN}be/api/admin/gallery/delete?${urlString}`,
      {},
      {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          timeout: TIMEOUT
      }
    ).then(res => {
      if(res.data.rc == "00"){
        alert.alertCreateEditSuccess(res.data.message, res.data.status)
        return 'success'
      }else{
        alert.alertCreateEditError(res.data.message, 'Data', 'Delete', res.data.status)
        return 'error'
      }
    }).catch(err => {
      alert.alertCreateEditError(err.response.data.message, 'Data', 'Delete', err.response.status)
      return 'error'
    })
  },

}
export { endpoint }
