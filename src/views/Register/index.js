import React, { useState } from 'react'
import { post } from 'axios'
import config from '../../services/config'
import urlLogo from '../../global/assets/logo.png'
import { Row, Col, Input, Button, notification, Card, Form } from 'antd'
import { LockOutlined, UserOutlined, MailOutlined, PhoneOutlined } from '@ant-design/icons'
import {
  Link, useHistory
} from "react-router-dom";

import './index.scss';

const { BASE_URL } = config

function Register (props) {
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();
  const history = useHistory();

  const handleFinish = () => {
    // mengambil value dari form
    form.validateFields().then(_ => {
      const { fullName, noHandphone, email, password, confirmPassword } = form.getFieldsValue();
      // menyamakan pasword dengan confirmPassword
      if(password !== confirmPassword){
        // menampilkan error jika passowrd tidak sama dengan confirmPassword
        notification.error({
          placement: 'bottomRight',
          message: 'Error',
          description: "Password dan Konfirmasi Password tidak sama!",
        });
      }else{
        setIsLoading(true);
        // hit API untuk mendaftarkan akun user
        post(`${BASE_URL}rest/web/open/auth/register`, {
          email : email,
          password : password,
          nama : fullName,
          noTelp : noHandphone
        })
        .then(res => {
          let data = res && res.data && res.data;
          if (data.status === "success") {
            // hit API untuk mengirimkan email verifikasi
            post(`${BASE_URL}rest/web/open/auth/kirim-verikasi`, {
              email : email,
            })
            .then(res => {
              let data = res && res.data && res.data;
              if (data.status === "success") {
                notification.success({
                  placement: 'bottomRight',
                  message: 'Sukses',
                  description: 'Berhasil daftar user, cek email untuk verifikasi',
                });
                // reset form
                form.resetFields();
              }else{
                notification.error({
                  placement: 'bottomRight',
                  message: 'Error',
                  description: res.data ? res.data.info.message : 'Gagal daftar user!',
                });
              }
              setIsLoading(false);
            })
            .catch(error => {
              notification.error({
                placement: 'bottomRight',
                message: 'Error',
                description: error.data
                  ? error.data.info.message
                  : 'Gagal daftar user!',
              });
              setIsLoading(false);
            })
          }else{
            notification.error({
              placement: 'bottomRight',
              message: 'Error',
              description: res.data ? res.data.info.message : 'Gagal daftar user!',
            });
            setIsLoading(false);
          }
        })
        .catch(error => {
          notification.error({
            placement: 'bottomRight',
            message: 'Error',
            description: error.data
              ? error.data.info.message
              : 'Gagal daftar user!',
          });
          setIsLoading(false);
        })
      }
    }).catch(err => {
      return err
    })
  }

  const handleLogin = () => {
    // mengganti route path ke login
    history.push(`/login`)
  }

  return (
    <div className="content">
      <div className="container">
        <div style={{display: !props.isMobile && 'flex', alignItems: 'center', height: '100%'}}>
          <Row className="login-row" gutter={[20, 20]} style={{margin: props.isMobile && '0', background: '#EDEDED'}} justify='center'>
            <Col span={24}>
              <Row justify='center'>
                <Col>
                    <img className="defaultLogo" alt="defaultLogo" src={urlLogo}/>
                </Col>
              </Row>
              <Row justify='center'>
                <Col lg={8} md={12} className="contentainer">
                  <Card className="app-card" style={{padding: '0px 10px'}}>
                    <Form name="formLogin" form={form} onFinish={values => handleFinish()}>
                      <div className="login-title">Mari mendaftarkan diri...</div>
                      <Row>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Nama Lengkap
                        </Col>
                        <Col span={24} style={{ marginBottom: '15px' }}>
                          <Form.Item
                            name='fullName'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'Nama lengkap harus diisi!'}]}
                          >
                            <Input
                              prefix={<UserOutlined />}
                              placeholder="Masukan Nama Lengkap"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          No Handphone
                        </Col>
                        <Col span={24} style={{ marginBottom: '15px' }}>
                          <Form.Item
                            name='noHandphone'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'No Handphone harus diisi!'}]}
                          >
                            <Input
                              prefix={<PhoneOutlined />}
                              placeholder="Masukan No Handphone"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          E-mail
                        </Col>
                        <Col span={24} style={{ marginBottom: '15px' }}>
                          <Form.Item
                            name='email'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'E-mail Harus diisi!'}]}
                          >
                            <Input
                              prefix={<MailOutlined />}
                              placeholder="Masukan E-mail"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Password
                        </Col>
                        <Col span={24} style={{ marginBottom: '15px' }}>
                          <Form.Item
                            name='password'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'Password Harus diisi!'}]}
                          >
                            <Input.Password
                              prefix={<LockOutlined />}
                              placeholder="Masukan Password"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Konfirmasi Password
                        </Col>
                        <Col span={24}>
                          <Form.Item
                            name='confirmPassword'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'Konfirmasi Password Harus diisi!'}]}
                          >
                            <Input.Password
                              prefix={<LockOutlined />}
                              placeholder="Masukan Konfirmasi Password"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row>
                            <Col span={8}>
                            </Col>
                            <Col span={16} style={{ textAlign: 'right'}}>
                              <Button
                                htmlType='submit'
                                size="large"
                                className="app-btn primary"
                                style={{backgroundColor:"#00C3D4",fontWeight:"bold",border:"none"}}
                                type="primary"
                                loading={isLoading}
                                >
                                Daftar
                              </Button>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row style={{padding:0,margin:0}}>
                            <Col style={{padding:0,margin:0}} span={16}>
                              Sudah punya akun ? <Link onClick={handleLogin} className="daftarDisini">Masuk disini</Link>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Form>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}


export default Register
