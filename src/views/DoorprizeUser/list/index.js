import React, { useEffect, useState } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Row,
  Col,
	Input,
	Spin,
	Pagination 
} from 'antd'
import { endpoint } from '../../../api/doorprizeUser';
import Meta from 'antd/lib/card/Meta';
import {SearchOutlined} from '@ant-design/icons'
import debounce from 'lodash.debounce'
import doorImage from '../../../global/assets/doorPrizeDefault.png'

const DoorprizeUser = props => {
	const delay = 1000
	let debouncedFn;

	const [dataHadiah, setDataHadiah] = useState([])
	const [isLoading, setLoading] = useState(true)
	const [pagination, setPagination] = useState({
		current: 1,
		total: 0,
		pageSize: 6
	})
	const [body, setBody] = useState({
		search: '',
		size: 6,
		page: 1,
		token: props.token
	})

	useEffect(() => {
		// memanggil fungsi getDoorprize setiap kali state body berubah
		getDoorprize(body)
	},[body])

	const getDoorprize = param => {
		// get data doorprize user
		trackPromise(
      endpoint.getDoorprize(param).then(res => {
				setPagination({
					...pagination,
					total: res.totalElements
				})
			 	setDataHadiah(res.content)
				setLoading(false)
      })
    )
	}

	
	// mendelay fungsi dengan async agar saat memanggil API tidak terjadi penumpukan request API
	const handleChange = async (info) => {
		// memanggil data API setiap kali user menginputkan data ke kolom pencarian
		setLoading(true)
		const keyword = info.target.value;
		if (!debouncedFn) {
			// delay change value dengan parameter delay yang telah di set di sebelumnya(1000)
			debouncedFn = debounce(async (val) => {
			onChange(val);
		  }, delay);
		}
		await debouncedFn(keyword);
	}

	const onChange = val => {
		// merubah state body dengan value pada parameter
		setBody({
			...body,
			search: val
		})
	}

	const handlePagination = (page, size) => {
		// merubah page berdasarkan parameter
		setPagination({
			...pagination,
			current: page
		})
		setBody({
			...body,
			page: page
		})
	}

  return(
    <div className='root'>
			<Spin spinning={isLoading}>
				<Row gutter={[4,4]}>
					<Col span={24} style={{marginBottom: '40px'}}>
						<Col span={props.isMobile ? 24 : 8}>
							<Input 
								placeholder='Cari nama acara' 
								style={{borderRadius: '8px', border: '1px solid #00C3D4'}} 
								suffix={<SearchOutlined color='#333333'/>}
								onChange={handleChange}
							/>
						</Col>
					</Col>
					{dataHadiah.length > 0 ? (
						dataHadiah.map((res, index) => (
							<Col span={8} xs={24} md={8} key={index + 'col' + res.idAcara}>
								<Card
									className='cardContentDoor'
									cover={<img alt="example" src={doorImage} />}
								>
									<Meta
										title={res.jenis}
									>
									</Meta>
									<Row gutter={[0, 8]}>
										<Col span={24}>
											<span className="detailAcara">
												{res.namaAcara}
											</span>
										</Col>
										<Col span={12} style={{fontWeight: 'bold'}}>
											<span className="deskAcara">
												Jenis: 
											</span>
										</Col>
										<Col span={12}>
											<span className="deskAcara">
												{res.jenis}
											</span>
										</Col>
										<Col span={12} style={{fontWeight: 'bold'}}>
											<span className="deskAcara">
												No Resi : 
											</span>
										</Col>
										<Col span={12}>
											<span className="deskAcara">
												{res.noResi}
											</span>
										</Col>
										<Col span={12} style={{fontWeight: 'bold'}}>
											<span className="deskAcara">
												Status : 
											</span>
										</Col>
										<Col span={12}>
											<span className="deskAcara">
												{res.status}
											</span>
										</Col>
									</Row>
								</Card>
							</Col>
						))
					) : (
						<Col span={24} style={{textAlign: 'center'}}>
							<span className='descTextAcaraBold' style={{fontSize: '32px'}}>
								No Data
							</span>
						</Col>
					)}
					{dataHadiah.length > 0 && (
						<Col span={24}
							style={{marginTop: '30px', marginBottom: '30px', textAlign:'center'}}
						>
							<Pagination 
								current={pagination.current} 
								total={pagination.total} 
								pageSize={pagination.pageSize}
								onChange={(page, pageSize) => handlePagination(page, pageSize)}
							/>
						</Col>
					)}
				</Row>
			</Spin>
    </div>
  )
}



export default DoorprizeUser