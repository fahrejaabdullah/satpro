import React, { useEffect, useState } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Button,
  Row,
  Col,
	Tag,
	Input,
	Spin,
	Pagination,
	Popconfirm
} from 'antd'
import { endpoint } from '../../../api/acaraSaya';
import Meta from 'antd/lib/card/Meta';
import {SearchOutlined} from '@ant-design/icons'
import debounce from 'lodash.debounce'
import moment from 'moment';

const text = 'Apakah anda yakin akan membatalkan acara ini?'

const currentDate = new Date

const AcaraSaya = props => {
	const delay = 1000
	let debouncedFn;

	const [dataAcara, setDataAcara] = useState([])
	const [isLoading, setLoading] = useState(true)
	const [pagination, setPagination] = useState({
		current: 1,
		total: 0,
		pageSize: 6
	})
	const [body, setBody] = useState({
		search: '',
		size: 6,
		page: 1,
		token: props.token
	})

	useEffect(() => {
		// memanggil fungsi getData setiap kali state body berubah
		getData(body)
	},[body])

	const getData = param => {
		// get data acara dari API
		trackPromise(
      endpoint.getAcara(param).then(res => {
				// set pagination 
				setPagination({
					...pagination,
					total: res.totalElements
				})
				// set data acara yang di kirim oleh BE
			 	setDataAcara(res.content)
				// set loading false
				setLoading(false)
      })
    )
	}
	
	// ganti route path ke detail acara
	const handleDetail = id => {
		props.history.push(`acara-saya/${id}`)
	}

	// mendelay fungsi dengan async agar saat memanggil API tidak terjadi penumpukan request API
	const handleChange = async (info) => {
		// memanggil data API setiap kali user menginputkan data ke kolom pencarian
		setLoading(true)
		const keyword = info.target.value;
		if (!debouncedFn) {
			// delay change value dengan parameter delay yang telah di set di sebelumnya(1000)
			debouncedFn = debounce(async (val) => {
			onChange(val);
		  }, delay);
		}
		await debouncedFn(keyword);
	}

	const onChange = val => {
		// merubah state body dengan value pada parameter
		setBody({
			...body,
			search: val
		})
	}

	const handlePagination = (page, size) => {
		// merubah page berdasarkan parameter
		setPagination({
			...pagination,
			current: page
		})
		setBody({
			...body,
			page: page
		})
	}

	const handleReject = (id) => {
		// hit API reject berdasarkan id dari acara yang di pilih user
		endpoint.deleteData(props.token, id).then(() => {
			// mengembalikan value default dari state body
			setBody({
				search: '',
				size: 6,
				page: 1,
				token: props.token
			})
		})
	}

	const handleDisable = tanggal => {
		// disable button register jika tanggal acara sudah terlewat
		return moment(tanggal).isBefore(currentDate)
	}

  return(
    <div className='root'>
			<Spin spinning={isLoading}>
				<Row gutter={[4,4]}>
					<Col span={24} style={{marginBottom: '40px'}}>
						<Col span={props.isMobile ? 24 : 8}>
							<Input 
								placeholder='Cari nama acara' 
								style={{borderRadius: '8px', border: '1px solid #00C3D4'}} 
								suffix={<SearchOutlined color='#333333'/>}
								onChange={handleChange}
							/>
						</Col>
					</Col>
					{dataAcara.length > 0 ? (
						dataAcara.map((res, index) => (
							<Col span={8} xs={24} md={8} key={index + 'col' + res.idAcara}>
								<Card
									className='cardAcara'
									cover={<img alt="example" src={res.banner} />}
								>
									<Meta
										title={res.jenis}
									>
									</Meta>
									<Row gutter={[0, 8]}>
										<Col span={24}>
											<span className="detailAcara">
												{res.nama}
											</span>
										</Col>
										<Col span={24} style={{minHeight: '80px', maxHeight: '80px', overflow: 'hidden'}}>
											<pre className="deskAcara">
												{res.deskripsi}
											</pre>
										</Col>
										<Col span={12}>
											<Tag color="#202025">
												{res.tanggal}
											</Tag>
										</Col>
										<Col span={12}>
											<Tag color="#202025">
												{res.jam}
											</Tag>
										</Col>
										<Col span={8}>
											<Tag color="#202025">
												{res.contactPerson}
											</Tag>
										</Col>
										{res.isRegistered && 
											<Col span={24} style={{marginTop: '16px'}}>	
												{handleDisable(res.tanggal) ? (
													<Button 
														className="cancelButton"
														disabled 
													>
														{'X'} Tidak Hadir 
													</Button>
												) : (
													<Popconfirm
														placement="bottomRight"
														title={text}
														onConfirm={() => handleReject(res.idAcara)}
														okText="Yes"
														cancelText="No"
													>
															<Button className="cancelButton" >
																{'X'} Tidak Hadir 
															</Button>
													</Popconfirm>
												)}
											</Col>
										}
										<Col span={24} style={{marginTop: res.isRegistered ? '16px' : '88px'}}>
											<Button className="detailButton" onClick={() => handleDetail(res.idAcara)}>
												Lihat Detail {'>'}
											</Button>
										</Col>
									</Row>
								</Card>
							</Col>
						))
					) : (
						<Col span={24} style={{textAlign: 'center'}}>
							<span className='descTextAcaraBold' style={{fontSize: '32px'}}>
								No Data
							</span>
						</Col>
					)}
					{dataAcara.length > 0 && (
						<Col span={24}
							style={{marginTop: '30px', marginBottom: '30px', textAlign:'center'}}
						>
							<Pagination 
								current={pagination.current} 
								total={pagination.total} 
								pageSize={pagination.pageSize}
								onChange={(page, pageSize) => handlePagination(page, pageSize)}
							/>
						</Col>
					)}
				</Row>
			</Spin>
    </div>
  )
}



export default AcaraSaya