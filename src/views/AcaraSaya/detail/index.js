import React, { useEffect, useState } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Row,
  Col,
	Spin,
} from 'antd'
import { endpoint } from '../../../api/katalogAcaraUser';

const AcaraSayaDetail = props => {

	const [isLoading, setLoading] = useState(true)
	const [data, setData] = useState({})

	useEffect(() => {
		// set id dan token saat pertama kali page di buka
		const body = {
			id: props.location.pathname.split('/')[2],
			token: props.token
		}
		getData(body)
	},[])

	const getData = body => {
		// get data acara detail
		trackPromise(
      endpoint.getAcaraDetail(body).then(res => {
				setData(res.data)
				setLoading(false)
      })
    )
	}

	const handleDirect = url => {
		// membuka tab pembayaran midtrans
		window.open(url, '_blank')
	}

  return(
    <div className='root'>
			<Spin spinning={isLoading}>
				<Row gutter={props.isMobile ? [16,16] :[4,4]}>
					<Col span={24} style={{marginBottom: '40px'}}>
						<Card 
							className='containCardDetail'
						>
							<Row>
								<Col span={props.isMobile ? 24 : 12}>
									<Card
										className='cardDetail'
										cover={<img alt="example" className='imageCardDetail' src={data.banner} />}
									>
									</Card>
								</Col>
								<Col span={props.isMobile ? 24 :12}>
									<Row gutter={props.isMobile ? [16, 16] :[0, 8]}>
										<Col span={24}>
											<span className="detailAcaraCard">
												{data.nama}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Deskripsi : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.deskripsi}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Jenis Acara : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.jenis}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Narasumber : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.narasumber}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Hari / Tanggal : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.tanggal}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Jam : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.jam}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Media : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.media}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Biaya Pendaftaran : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.biayaPendaftaran}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Contan Person : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.contactPerson}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												{data.media === 'Online ' ? 'Link Pelaksanaan: ' : 'Alamat: '} 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											{data.media === 'Online' ? (
												<span className='descTextAcaraBold' style={{color: 'blue', wordBreak: 'break-word', cursor: 'pointer'}} onClick= {() => handleDirect(data.linkPelaksanaan)}>
													{data.linkPelaksanaan}
												</span>
												):(
												<span>
													{data.alamat}
												</span>
											)}
										</Col>
									</Row>
								</Col>
								<Col span={18}>
								</Col>
							</Row>
						</Card>
					</Col>
				</Row>
			</Spin>
    </div>
  )
}



export default AcaraSayaDetail