import React, { useEffect, useState } from "react";
import {
  Row, 
  Col, 
  Button, 
  Form,
  Input, 
  Card, 
  DatePicker, 
  Typography,
  Select,
  notification
} from "antd";
import "../../../global/global.css";
import findName from "../../../functions/findNameFile";
import TextArea from 'antd/lib/input/TextArea';
import UploadComponentValues from "../../../components/uploadComponentValues";
import moment from 'moment';
import { RedStar } from '../../../components';
import { services } from "../../../api/user";
import { trackPromise } from 'react-promise-tracker';


const {Text} = Typography
const { Option } = Select

function FormUpdateProfile(props) {
	const [form] = Form.useForm();
  const [loadingSubmit, setLoadingSubmit] = useState(false)
  const [defaultBanner, setDefaultBanner]= useState([])
  const currentRole = sessionStorage.getItem('userRole') ?? localStorage.getItem("userRole");
  const userData= sessionStorage.getItem('userData') ? sessionStorage.getItem('userData') : localStorage.getItem('userData') ? localStorage.getItem('userData') : null;
  const [isValid, setIsValid] = useState(false);
  const [email,setEmail] = useState("");

  useEffect(() => {
    // set data user setiap kali data userData berubah
    var _userData = JSON.parse(userData);
    setIsValid(_userData.verified === 1);
  },[userData]);

  useEffect(() => {
    // set data initial setiap kali props.initialData berubah
    if(props.initialData && props.isEdit){
      // maping data yang dikirimkan oleh parent jika props.isEdit bernilai true
      mappingValueDetail(props.initialData)
    }
  }, [props.initialData])

  const mappingValueDetail = e => {
    // maping data, agar pada saat pertama kali user masuk kedalam page form sudah terisi
    setEmail(sessionStorage.getItem("userId") ?? localStorage.getItem("userId"));
    let body = {
      email:sessionStorage.getItem("userId") ?? localStorage.getItem("userId"),
      password:e.password,
      confirmPassword:e.password,
      nama:e.nama,
      noTelp:e.noTelp,
      alamat:e.alamat,
      jenisKelamin:e.jenisKelamin,
      foto:e.foto,
      hakAkses:e.hakAkses,
    }
    if(e.tanggalLahir != null){
      body.tanggalLahir = moment(e.tanggalLahir);
    }
    form.setFieldsValue(body)
    if(e.foto != null){
      const fileName = findName(e.foto)
      setDefaultBanner([
        {
          uid: sessionStorage.getItem("userId") ?? localStorage.getItem("userId"),
          name: fileName,
          status: 'done',
          url: e.foto,
          thumbUrl: e.foto
        },
      ]);
    }
    return body
  }

	const onFinish = async(values) => {
    if(values.password !== values.confirmPassword){
      // menampilkan error jika passowrd tidak sama dengan confirmPassword
      notification.error({
        placement: 'bottomRight',
        message: 'Error',
        description: 'Password dan Konfirmasi Password harus sama!',
      });
    }else{
      // normalisasi object yang akan di kirimkan ke BE 
      var param = {
        "email" :values.email,
        "password" :values.password,
        "confirmPassword" :values.confirmPassword,
        "nama" :values.nama,
        "noTelp" :values.noTelp,
        "tanggalLahir" :moment(values.tanggalLahir).format("yyyy-MM-DD"),
        "alamat" :values.alamat,
        "jenisKelamin" :values.jenisKelamin,
        "foto" :values.foto,
        "hakAkses" :values.hakAkses
      };
      props.onFinish(param);
    }
	}

	const onFinishFailed = error => {
    // menampilkan pesan error jika form tidak di isi dengan benar
    console.log(error)
	}
  const disabledDate = (current) => {
    // disable tanggal yang telah melawati hari ini
    return current > moment().endOf('day');
  }

  const handledataChange = data => {
    // set data image yang di upload oleh user
    form.setFieldsValue({
        foto: (data?.fileList[0]?.response?.data)
    })
  }

  const sendEmail =()=>{
    var token= sessionStorage.getItem('token') ? sessionStorage.getItem('token') : localStorage.getItem('token') ? localStorage.getItem('token') : null;
    var param = {
      email:email
    };
    setLoadingSubmit(true);
    // send email verifikasi
    trackPromise(
      services.verifikasi(token,param).then(res => {
        setLoadingSubmit(false)
      })
    )
  }

  const CardHeader = () => {
    return (
      <Card className='bodyCard_style'>
        <Card
          bordered={false}
          style={{minHeight: '300px'}}>
            <Row gutter={[30]}>
              {
                !isValid?
                  <Col span={24} style={{textAlign:"end"}}>
                    <Button 
                      type="primary"
                      className='blueButton'
                      size={'small'}
                      onClick={sendEmail}
                      loading={loadingSubmit}>
                        Kirim Email
                    </Button>
                  </Col> 
                :<></>
              }
            <Col span={props.isMobile ? 24 : 12} >
                <Text className="form-label">Foto Profil <RedStar /></Text>
                <Form.Item name='foto'
                  rules={[{required: true,message: 'File Cover harus di-upload!' }
                ]}>
                  <UploadComponentValues
                    pathName='foto'
                    urlAPI={'rest/admin/secured/user/upload'}
                    token={props.token}
                    name={'thumbailPath'}
                    placeholder='Upload Foto (max:1 file)'
                    accept=".jpg,.png,.jpeg"
                    onChange={handledataChange}
                    defaultBanner={defaultBanner}
                  />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">E-Mail <RedStar /></Text>
                <Form.Item name='email'
                  rules={[{required: true,message: 'E-Mail Acara harus diisi!' }
                ]}>
                  <Input
                    disabled={true}
                    placeholder={'Masukan e-mail'}
                    size="large"
                    className='input_style'
                    maxLength={256}
                  />
                </Form.Item>
              </Col>
              
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Nama User <RedStar /></Text>
                <Form.Item name='nama'
                  rules={[{required: true,message: 'Nama User harus diisi!' }
                ]}>
                    <Input
                    placeholder={'Masukan nama'}
                    size="large"
                    className='input_style'
                    maxLength={256}
                  />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">No Telp <RedStar /></Text>
                <Form.Item name='noTelp'
                  rules={[{required: true,message: 'No Telp harus diisi!' }
                ]}>
                    <Input
                    placeholder={'Masukan no telp'}
                    size="large"
                    className='input_style'
                    maxLength={256}
                  />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Jenis Kelamin</Text>
                <Form.Item name='jenisKelamin'>
                  <Select 
                    style={{  
                      textAlign: 'left' 
                    }}
                    placeholder='Pilih jenis kelamin'
                  >
                      <Option value="Laki-Laki" key="Laki-Laki">Laki-Laki</Option>
                      <Option value="Perempuan" key="Perempuan">Perempuan</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Tanggal Lahir</Text>
                <Form.Item name='tanggalLahir'>
                  <DatePicker 
                    placeholder={'mm/dd/yyyy, --:-- --'} 
                    className='input_style'
                    format={'dddd,DD-MM-YYYY'}
                    disabledDate={disabledDate}
                  />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Alamat</Text>
                <Form.Item name='alamat'>
                  <TextArea placeholder={'Masukan alamat'}  size="large" className='input_style' />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
              </Col>
            </Row>
            <Row gutter={[20]}>
              <Col span={24}>
                <Form.Item
                  style={{
                    textAlign: 'right',
                    marginBottom: '0px'
                  }}
                >
                  <Button
                    type="primary"
                    htmlType='submit'
                    className='blueButton'
                    size={'small'}
                    loading={loadingSubmit}
                  >
                    Tambah
                  </Button>
                </Form.Item>
              </Col>
            </Row>
        </Card>
      </Card>
    )
  }

	return (
      <Form
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
      >
        <Row gutter={[30,30]}>
          <Col span={24}>
            <CardHeader/>
          </Col>
          {/* <pre>
            {JSON.stringify(listDataFile)}
          </pre> */}
        </Row>
    </Form>
	)
}

export const UIFormUpdateProfile = FormUpdateProfile;
