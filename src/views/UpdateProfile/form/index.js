import React, {Component} from 'react'
import '../../../global/global.css'
import { services } from '../../../api/user'
import { trackPromise } from 'react-promise-tracker';
import {
  Spin,
  Typography,
} from 'antd'
import { UIFormUpdateProfile } from './component';

const { Text } = Typography
class UpdateProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lovJenisPekerjaan: [],
      loading: false,
      isEdit: false,
      initialData: {},
      currentRole:"",
      eventId: null
    }
  }

  componentDidMount() {
    const id = this.props.location.pathname.split('/')[2]
    const type = this.props.location.pathname.split('/')[1]
    const currentRole = sessionStorage.getItem('userRole') ?? localStorage.getItem("userRole");
    this.setState({
      currentRole:currentRole,
    });
    if(type == 'update-profile-admin' && id){
      const body = {
        token: this.props.token,
        id: id
      }
      this.setState({
        eventId: id,
        loading: true,
        isEdit: true
      }, () => this.getDataId(body))
    }else{
      const body = {
        token: this.props.token,
        id: id
      }
      this.setState({
        loading: true,
        isEdit: true
      }, () => this.getDataUser(body))
    }
  }

  getDataId = param => {
    // get data detail profile
		trackPromise(
      services.getDataDetail(param).then(res => {
        let dataTemp = {
          ...res.data.data
        };
        
        this.setState({
          initialData: dataTemp,
          loading: false
        })
      })
    )
  }

  getDataUser = param => {
    // get data user
		trackPromise(
      services.getDataUser(param).then(res => {
        let dataTemp = {
          ...res.data.data
        }
        this.setState({
          initialData: dataTemp,
          loading: false
        })
      })
    )
  }

  editHeaderFooter = (data) => {
    let body = {
      ...data,
      usersId: this.props.userId
    }
    if(this.state.isEdit){
      if(this.state.currentRole == "User"){
        // edit data profile user
        trackPromise(
          services.editUser(this.props.token, body).then((res) => {
            if(res == 'success'){
              this.props.history.push('/')
            }
          })
        )
      }else{
        // edit data profile berdasarkan id
        body.eventId = this.state.eventId
        body.banner = 
        trackPromise(
          services.edit(this.props.token, this.state.eventId, body).then((res) => {
            if(res == 'success'){
              this.props.history.push('/dashboard')
            }
          })
        )
      }
    }
  }

  render(){
    const { lovJenisPekerjaan, loading, isEdit, initialData } = this.state
    return (
      <div className='root'>
        <Spin spinning={loading}>
          <UIFormUpdateProfile 
            data={lovJenisPekerjaan}
            onFinish={(e)=>this.editHeaderFooter(e)}
            getLov={e => this.getData(e)}
            token = {this.props.token}
            isMobile={this.props.isMobile}
            isEdit = {isEdit}
            initialData = {initialData}
          />
        </Spin>
      </div>
    )
  }
}


export default UpdateProfile
