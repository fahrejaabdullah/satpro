import React, { useState,useEffect } from 'react'
import { post } from 'axios'
import config from '../../services/config'
import urlLogo from '../../global/assets/logo.png'
import { Row, Col, Card } from 'antd'
import { CheckCircleOutlined  } from '@ant-design/icons'
import {
  Link, useHistory,useParams
} from "react-router-dom";

import './index.scss';

const { BASE_URL } = config

function Verifikasi (props) {
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();
  const {email} = useParams();

  const handleRoute = (route) => {
      history.push(route)
  }

  const handleFinish = (emailUser) => {
    // verifikasi user berdasarkan email
    setIsLoading(true);
    post(`${BASE_URL}rest/web/open/auth/verifikasi?email=`+emailUser, )
    .then(res => {
      setIsLoading(false);
    })
    .catch(error => {
      setIsLoading(false);
    })
  }

  useEffect(() => {
    sessionStorage.clear();
    localStorage.clear();
    handleFinish(email);
  }, []);
  


  return (
    <div className="content">
      <div className="container">
        <div style={{display: 'flex', alignItems: 'center', height: '100%'}}>
          <Row className="login-row" gutter={[20, 20]} justify='center'>
            <Col span={24}>
              <Row justify='center'>
                <Col>
                    <img className="defaultLogo" alt="defaultLogo" src={urlLogo}/>
                </Col>
              </Row>
              <Row justify='center'>
                <Col lg={12} md={16} className="contentainer">
                  <Card className="app-card" style={{padding: '0px 10px'}}>
                      <Col span={24}>
                        <CheckCircleOutlined style={{color:"#00C3D4",fontSize:80}} />
                      </Col>
                      <Col span={24} style={{marginTop:10}}>
                        <div style={{fontSize:20,fontWeight:700}}>
                            Akun anda telah berhasil ter-verifikasi
                        </div>
                      </Col>
                      <Col span={24} style={{marginTop:20}}>
                          <Row justify="center">
                            <Link onClick={()=>handleRoute("/login/")}>
                                <div className="buttonLogin">Login</div>
                            </Link>
                          </Row>
                      </Col>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}


export default Verifikasi
