import React, { useState } from 'react'
import { post } from 'axios'
import config from '../../services/config'
import urlLogo from '../../global/assets/logo.png'
import { Row, Col, Input, Button, notification, Card, Form,Checkbox } from 'antd'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import {
  Link, useHistory
} from "react-router-dom";

import './index.scss';

const { BASE_URL } = config

function LandingPage (props) {
  const [isLoading, setIsLoading] = useState(false);
  const [isRemember, setIsRemember] = useState(false);
  const [form] = Form.useForm();
  const history = useHistory();
  
  const saveUserData = (data) => {
    if(isRemember){
      localStorage.setItem('token', data.token)
      localStorage.setItem('userData', JSON.stringify(data.user))
      localStorage.setItem('name', data.user.nama)
      localStorage.setItem("userId", data.user.email);
      localStorage.setItem("isRemeber", true)
    }else{
      sessionStorage.setItem('token', data.token)
      sessionStorage.setItem('userData', JSON.stringify(data.user))
      sessionStorage.setItem('name', data.user.nama)
      sessionStorage.setItem("userId", data.user.email);
      sessionStorage.setItem("isRemeber", false)
    }
  }

  const handleFinish = () => {
    form.validateFields().then(_ => {
      const { username, password } = form.getFieldsValue();
      setIsLoading(true);
      // login user
      post(`${BASE_URL}authenticate`, {
        username: username,
        password: password,
        isRemember: isRemember
      })
      .then(res => {
        let data = res && res.data && res.data;
        const roleGroup = data && data.user && data.user.hakAkses;
        if (roleGroup === "admin") {
          // set data jika role akun 'Admin'
          saveUserData(data);
          if(isRemember){
            // save data ke local storage jika user memilih remember me
            sessionStorage.clear();
            localStorage.setItem("userRole",'Admin');
          }else{
            // save data ke session storage jiga user tidak memilih remember me
            sessionStorage.setItem('userRole', 'Admin')
          }
          const redirection = {
            'Admin': '/dashboard',
          }
          history.push(redirection[roleGroup]);
        }else if (roleGroup === "user"){
          // set data jika role akun 'User'
          saveUserData(data);
          if(isRemember){
            // save data ke local storage jika user memilih remember me
            sessionStorage.clear();
            localStorage.setItem("userRole",'User');
          }else{
            // save data ke session storage jiga user tidak memilih remember me
            sessionStorage.setItem('userRole', 'User')
          }
          const redirection = {
            'User': '/',
          }
          history.push(redirection[roleGroup]);
        }else{
          notification.error({
            placement: 'bottomRight',
            message: 'Error',
            description: 'Email atau Password salah!',
          });
        }
        setIsLoading(false);
      })
      .catch(error => {
        notification.error({
          placement: 'bottomRight',
          message: 'Error',
          description: error.data
            ? error.data.Message
            : 'Email atau Password salah!',
        });
        setIsLoading(false);
      })
    })
  }


  const handleForgotPassword = () => {
    // mengganti route path ke forget password
    history.push(`/ForgetPassword/`)
  }

  const handleRegister = () => {
    // mengganti route path ke register
    history.push(`/register/`)
  }

  const onChangeRememberMe = () =>{
    // set value remember me
    setIsRemember(!isRemember);
  }

  return (
    <div className="content">
      <div className="container">
        <div 
          style={{
            display: !props.isMobile && 'flex', 
            alignItems: 'center', 
            height: '100%'
          }}
        >
          <Row className="login-row" gutter={[20,20]} style={{margin: props.isMobile && '0', background: '#EDEDED'}} justify='center'>
            <Col span={24}>
              <Row justify='center'>
                <Col>
                    <img className="defaultLogo" alt="defaultLogo" src={urlLogo}/>
                </Col>
              </Row>
              <Row justify='center'>
                <Col lg={8} md={12} className="contentainer">
                  <Card className="app-card" style={{padding: '0px 10px'}}>
                    <Form name="formLogin" form={form} onFinish={values => handleFinish()}>
                      <div className="login-title">Selamat Datang...</div>
                      <Row>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                        E-Mail
                        </Col>
                        <Col span={24} style={{ marginBottom: '15px' }}>
                        <Form.Item
                          name='username'
                          style={{marginBottom: '0em'}}
                          rules={[{required: true, message: 'E-Mail Harus diisi!'}]}
                        >
                          <Input
                            prefix={<UserOutlined />}
                            placeholder="E-Mail"
                            className="input-style-login"
                          />
                        </Form.Item>
                        </Col>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Password
                        </Col>
                        <Col span={24} style={{ marginBottom: '5px' }}>
                          <Form.Item
                            name='password'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'Password Harus diisi!'}]}
                          >
                            <Input.Password
                              prefix={<LockOutlined />}
                              placeholder="Password"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row>
                            <Col span={10}>
                              <Checkbox onChange={onChangeRememberMe}>Remember me</Checkbox>
                            </Col>
                            <Col span={14} style={{ textAlign: 'right'}}>
                              <Button
                                htmlType='submit'
                                size="large"
                                className="app-btn primary"
                                style={{backgroundColor:"#00C3D4",fontWeight:"bold",border:"none"}}
                                type="primary"
                                loading={isLoading}
                                >
                                Masuk
                              </Button>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row style={{padding:0,margin:0}}>
                            <Col style={{padding:0,margin:0}} span={16}>
                              Belum punya akun ? <Link onClick={handleRegister} className="daftarDisini">Daftar disini</Link>
                            </Col>
                            <Col style={{padding:0,margin:0}} span={8} style={{ textAlign: 'right', marginBottom: '30px'}}>
                              <Link onClick={handleForgotPassword} className="lupa-password-style">Lupa Password ?</Link>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Form>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}


export default LandingPage
