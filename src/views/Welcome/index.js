import React, { useEffect, useState } from 'react'
import urlLogo from '../../global/assets/logo.png'
import urlWelcome from '../../global/assets/selamat_datang.png'
import urlDonasiPalestin from '../../global/assets/donasi_palestin.png'
import urlKatalogAcara from '../../global/assets/katalog_acara.png'
import urlLiveStream from '../../global/assets/live_stream.png'
import urlProfil from '../../global/assets/profil_welcome.png'
import { Row, Col,Menu,Dropdown  } from 'antd'
import { DownOutlined, PlusCircleOutlined, PhoneOutlined, YoutubeOutlined, MailOutlined} from '@ant-design/icons'
import { Link, useHistory } from "react-router-dom";
import ModalUbahPassword from "../../components/modalUbahPassword";

import './index.scss';
import ModalDonasiPalestina from '../../components/modalDonasiPalestina';

function Welcome (props) {
  const token = props.token;
  const history = useHistory();
  const [showModal, setShowModal] = useState(false);
  const [showModalUbahPassword, setShowModalUbahPassword] = useState(false);
  const [isValid, setIsValid] = useState(false);
  useEffect(() => {
    var _userData = JSON.parse(props.userData);
    if(_userData!=null){
      setIsValid(_userData.verified === 1);
    }
  },[props.userData]);
  const handleRoute = (route) => {
    history.push(route)
  }

  const toggleModal = () => {
    setShowModal(!showModal);
  }

  const onChangePass = () => {
    toggleModalUbahPassword();
    handleLogout();
  }

  const toggleModalUbahPassword = () => {
    setShowModalUbahPassword(!showModalUbahPassword);
  }

  const menu = () => (
    <Menu>
      <Menu.Item onClick={toggleModalUbahPassword}>Ubah Password</Menu.Item>
      <Menu.Item onClick={handleUpdateProfile}>Update Profile</Menu.Item>
      <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
    </Menu>
  )

  const handleUpdateProfile = () => {
    props.history.push("/update-profile");
  }

  const openYoutube = () => {
    const newWindow = window.open("https://www.youtube.com/channel/UCsFcQYNgh8B2oAHwHgqyhzA", '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }

  const handleKatalog = () => {
    props.history.push('/katalog-acara')
  }

  const handleLogout = () => {
    sessionStorage.clear(); 
    localStorage.clear();
    props.history.push("/login")
  }

  return (
    <div className="content">
      <Row>
        <Col span={24}>
          <div className="header">
            <Row>
              <Col span={props.isMobile ? 12 : 16}>
                <img className="imgHeader" src={urlLogo}/>
              </Col>
              {!props.isMobile && (
                <Col span={4}>
                </Col>
              )}
                <Col span={props.isMobile ? 12 : 4} style={{textAlign: 'right', paddingTop: props.isMobile ? 12 : 25}}>
                {props.name ? (
                  <Col span={24}>
                    <Col span={24}>
                      <Dropdown overlay={menu} placement="bottomCenter" arrow trigger={['click']}>
                        <Col span={24} style={{ cursor: 'pointer' }}>
                          <span>Hi, </span>
                          <span className="bold_text" style={{marginRight: '20px'}}>{props.name} <DownOutlined /></span>
                        </Col>
                      </Dropdown>
                    </Col>
                    {
                      !isValid ? (
                      <Col span={24}>
                        <div style={{marginRight:"20px", fontWeight:700,textAlign:"end"}}>
                          User belum terverifikasi
                        </div>
                      </Col>
                      ):<></>
                    }
                  </Col>

                ) : (
                  <></>
                )}
              </Col>
            </Row>
          </div>
        </Col>
        <Col span={24}>
          <div className="container">
            <div style={{display: 'flex', height: '100%'}}>
              <Col span={24}>
                <Row gutter={[20, 20]} style={!props.isMobile && {display: 'flex'}}>
                  <Col span={ props.isMobile ? 24 : 12 }>
                    <Col span={24} style={{marginTop:140}}>
                      <div className="titleWelcome">Selamat datang di <br/>EL QUR’AN TV</div>
                    </Col>
                    <Col span={24}>
                      <div className="textWelcome">
                        El Quran Tv merupakan Channel Media Dakwah Qurani yang merangkul segenap lapisan masyarakat. {'\n'}
                        Subscribe channel dan aktifkan notifikasi untuk mendapatkan ilmu dan informasi terbaru dari Kibar Ulama, Pakar Qiroat Al Quran dan Sekolah Tahfidz Intensif El Quran Tv
                      </div>
                    </Col>
                    <Col span={24}>
                      {
                        token !== null ?
                        <Row>
                        <Col span={12}>
                        </Col>
                        <Col span={12}>
                        </Col>
                      </Row>
                      : 
                        props.isMobile ? (
                          <Row justify="space-around">
                            <Col span={10}>
                              <Link onClick={()=>handleRoute("/register/")}>
                                <div className="buttonDaftarAkun">Daftar</div>
                              </Link>
                            </Col>
                            <Col span={10}>
                              <Link onClick={()=>handleRoute("/login/")}>
                                <div className="buttonLoginWelcome">Login</div>
                              </Link>
                            </Col>
                          </Row>
                        ) : (
                          <Row>
                            <Col span={12}>
                              <Link onClick={()=>handleRoute("/register/")}>
                                <div className="buttonDaftarAkun">Daftar</div>
                              </Link>
                            </Col>
                            <Col span={12}>
                              <Link onClick={()=>handleRoute("/login/")}>
                                <div className="buttonLoginWelcome">Login</div>
                              </Link>
                            </Col>
                          </Row>
                        )
                      }
                    </Col>
                  </Col>
                  <Col span={props.isMobile ? 24 : 12}>
                    <img className='welcomeImage' src={urlWelcome}/>
                  </Col>
                </Row>
                <Row style={{marginTop: props.isMobile ? 30 : 168}} gutter={props.isMobile && [24, 24]} justify="center">
                  <div className="titleLayanan">Layanan Kami</div>
                </Row>
                <Row style={{marginTop:10}} gutter={props.isMobile && [24, 24]} justify="center">
                  <div className="textWelcome" style={{textAlign:"center"}}>
                    El Quran Tv merupakan Channel Media Dakwah Qurani yang merangkul segenap lapisan masyarakat.<br/>
                    Bagi Sahabat yang ingin bekontribusi dalam bentuk Talk Show, Tausiah, Pembuatan Iklan, Barter program, Trading produk, Blocking time, Pembuatan Tayangan Program, Jurnalistic dan broadcasting termasuk pembuatan video kreativitas, in sya Allah akan kami bantu dengan Syarat tetap sesuai dengan norma dan religius islami
                  </div>
                </Row>
                <Row style={{marginTop:20, textAlign: props.isMobile && 'center'}} gutter={props.isMobile && [24, 24]}>
                  <Col span={props.isMobile ? 24 : 8}>
                    <Link onClick={()=>toggleModal()}>
                      <img className="cardLayanan backgroundCard" src={urlDonasiPalestin}/>
                    </Link>
                  </Col>
                  <Col span={props.isMobile ? 24 : 8}>
                    <Link onClick={()=>handleRoute("#")}>
                      <img className="cardLayanan" src={urlKatalogAcara} onClick={() => handleKatalog()}/>
                    </Link>
                  </Col>
                  <Col span={props.isMobile ? 24 : 8}>
                    <Link onClick={()=>openYoutube()}>
                      <img className="cardLayanan" src={urlLiveStream}/>
                    </Link>
                  </Col>
                </Row>
                <Row style={{marginTop: props.isMobile ? 40 : 240}} gutter={props.isMobile && [24, 24]}>
                  <Col span={props.isMobile ? 24 : 12}>
                    <img className="imageProfil" src={urlProfil}/>
                  </Col>
                  {!props.isMobile && 
                    (
                      <Col span={2}></Col>
                    ) 
                  }
                  <Col span={props.isMobile ? 24 : 10} style={{marginTop: props.isMobile && '30px'}}>
                    <div className="titleLayanan">
                      Profil EL QUR’AN TV
                    </div>
                    <br/>
                    <div className="textWelcome" style={{textAlign:"justify"}}>
                      Sahabat El Quran Tv dimana pun Antum berada, El Quran Tv merupakan Channel Dakwah Qurani dalam rangka pembelajaran Al Qur'an dan Kajian Islam bersama Para Masyaikh yang kredibel dibidangnya, sehingga diharapkan akan lahir generasi penerus Qurani dimasa yang akan datang.
                      <br/> <br/>
                      Muhammad Nur Hasan, LC
                      <br />
                      <span style={{ fontWeight: '600', color: '#7D7987', fontStyle: 'italic'}}>
                        Chief Executive Officer
                      </span>
                    </div>
                  </Col>
                </Row>
              </Col>
            </div>
          </div>
        </Col>
        <Col span={24} style={{marginTop:100}}>
          <div className="footer">
            <Col span={24}>
              <Row>
                <Col span={8}>
                  <Col span={24}>
                    <img className="imgFooter" src={urlLogo} style={{marginTop:40}}/>
                  </Col>
                  <Col>
                    <div className="copyright">
                      ©EL QUR’AN TV 2021. All rights reserved
                    </div>
                  </Col>
                </Col>
                <Col span={16} style={{marginTop:20}}>
                  <Col span={24}>
                    <Row>
                      <Col span={props.isMobile ? 4 : 16}>
                      </Col>
                      <Col span={props.isMobile ? 20 : 8}>
                        <Col span={24}>
                          <div className="titleFooter">
                            Kontak Kami
                          </div>
                        </Col>
                        <Col span={24} style={{marginTop:10}}>
                          <Row>
                            <Col span={2}>
                              <PlusCircleOutlined style={{color:"#f2f2f2"}}/>
                            </Col>
                            <Col span={22}>
                              <div className="textFooter">
                                Yayasan Bina Quran Bogor Jl Cidokom Lembur Gede, Desa Kopo, Kec. Cisarua, Bogor, Jawa barat
                              </div>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:10}}>
                          <Row>
                            <Col span={2}>
                              <PhoneOutlined style={{color:"#f2f2f2"}}/>
                            </Col>
                            <Col span={22}>
                              <div className="textFooter">
                                +62 852-1554-2666<br/>
                                +62 858-8170-9907
                              </div>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:10}}>
                          <Row>
                            <Col span={2}>
                              <YoutubeOutlined style={{color:"#f2f2f2"}}/>
                            </Col>
                            <Col span={22}>
                              <Link onClick={openYoutube}>
                                <div className="linkFooter">Youtube El Qur'an</div>
                              </Link>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:10}}>
                          <Row>
                            <Col span={2}>
                              <MailOutlined style={{color:"#f2f2f2"}}/>
                            </Col>
                            <Col span={22}>
                              <div className="linkFooter">binaqurana@gmail.com</div>
                            </Col>
                          </Row>
                        </Col>
                      </Col>
                    </Row>
                  </Col>
                </Col>
              </Row>
            </Col>
          </div>
        </Col>
      </Row>
      <ModalDonasiPalestina visible={showModal} onCancel={toggleModal} />
      <ModalUbahPassword visible={showModalUbahPassword} onCancel={toggleModalUbahPassword} onFinish={onChangePass} />
    </div>
  );
}


export default Welcome
