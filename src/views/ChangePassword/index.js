import React, { useState } from 'react'
import { post } from 'axios'
import config from '../../services/config'
import urlLogo from '../../global/assets/logo.png'
import { Row, Col, Input, Button, notification, Card, Form,Checkbox } from 'antd'
import { LockOutlined } from '@ant-design/icons'
import {
  Link, useHistory, useParams
} from "react-router-dom";

import './index.scss';

const { BASE_URL } = config

function ChangePassword () {
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();
  const history = useHistory();
  const {email} = useParams();

  const handleFinish = () => {
    // menggambil data dari form
    form.validateFields().then(_ => {
      const { password, confirmPassword } = form.getFieldsValue();
      // menyamakan pasword dengan confirmPassword
      if(password !== confirmPassword){
        // menampilkan error jika passowrd tidak sama dengan confirmPassword
        notification.error({
          placement:"bottomRight",
          message:"Error",
          description:"Password dan Konfirmasi Password harus sama!"
        });
      }else{
        setIsLoading(true);
        // mengirimkan passowrd baru yang telah di inputkan oleh user
        post(`${BASE_URL}rest/web/open/auth/change-password?email=`+email, {
          newPassword: password,
          confirmPassword: confirmPassword,
        })
        .then(res => {
          let data = res && res.data && res.data;
          if (data.status === "success") {
            notification.success({
              placement:"bottomRight",
              message:"Sukses",
              description:data.info.message,
            })
            // menreset isian dari form
            form.resetFields();
          }else{
            notification.error({
              placement: 'bottomRight',
              message: 'Error',
              description: 'Username atau Password salah!',
            });
          }
          setIsLoading(false);
        })
        .catch(error => {
          notification.error({
            placement: 'bottomRight',
            message: 'Error',
            description: error.data
              ? error.data.Message
              : 'Username atau Password salah!',
          });
          setIsLoading(false);
        })
      }
    }).catch(err => {
      return err
    })
  }

  const handleRoute = (route) => {
    // merubah route path menjadi login
    history.push(route)
  }

  return (
    <div className="content">
      <div className="container">
        <div style={{display: 'flex', alignItems: 'center', height: '100%'}}>
          <Row className="login-row" gutter={[20, 20]} justify='center'>
            <Col span={24}>
              <Row justify='center'>
                <Col>
                    <img className="defaultLogo" alt="defaultLogo" src={urlLogo}/>
                </Col>
              </Row>
              <Row justify='center'>
                <Col lg={8} md={12} className="contentainer">
                  <Card className="app-card" style={{padding: '0px 10px'}}>
                    <Form name="formLogin" form={form} onFinish={values => handleFinish()}>
                      <div className="login-title">Ganti Password</div>
                      <Row>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Password
                        </Col>
                        <Col span={24} style={{ marginBottom: '5px' }}>
                          <Form.Item
                            name='password'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'Password Harus diisi!'}]}
                          >
                            <Input.Password
                              prefix={<LockOutlined />}
                              placeholder="Password"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Konfirmasi Password
                        </Col>
                        <Col span={24} style={{ marginBottom: '5px' }}>
                          <Form.Item
                            name='confirmPassword'
                            style={{marginBottom: '0em'}}
                            rules={[{required: true, message: 'Konfirmasi Password Harus diisi!'}]}
                          >
                            <Input.Password
                              prefix={<LockOutlined />}
                              placeholder="Konfirmasi Password"
                              className="input-style-login"
                            />
                          </Form.Item>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row>
                            <Col span={24} style={{ textAlign: 'right'}}>
                              <Button
                                htmlType='submit'
                                size="large"
                                className="app-btn primary"
                                style={{backgroundColor:"#00C3D4",fontWeight:"bold",border:"none"}}
                                type="primary"
                                loading={isLoading}
                                >
                                Masuk
                              </Button>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row style={{padding:0,margin:0}}>
                            <Col style={{padding:0,margin:0}} span={24} style={{ textAlign: 'right', marginBottom: '30px'}}>
                              <Link onClick={()=>handleRoute("/login/")} className="lupa-password-style">Login disini</Link>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Form>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}


export default ChangePassword
