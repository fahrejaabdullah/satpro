import React, { useState } from 'react'
import { post } from 'axios'
import config from '../../services/config'
import urlLogo from '../../global/assets/logo.png'
import { Row, Col, Input, Button, notification, Card, Form } from 'antd'
import { UserOutlined } from '@ant-design/icons'
import { Link, useHistory } from "react-router-dom";
import './index.scss';

const { BASE_URL } = config

function ForgetPassword () {
  const [isLoading, setIsLoading] = useState(false);
  const [form] = Form.useForm();
  const history = useHistory();

  const handleFinish = () => {
    // mengambil value dari form
    form.validateFields().then(_ => {
      const { email} = form.getFieldsValue();
      setIsLoading(true);
      // mengirimkan email yang akan dikirimkan data passowd
      post(`${BASE_URL}rest/web/open/auth/forgot-password`, {
        email:email,
      })
      .then(res => {
        let data = res && res.data && res.data;
        if(data.status === "success"){
          notification.success({
            placement:"bottomRight",
            message:"Sukses",
            description:data.info.message
          });
        }else{
          notification.error({
            placement:"bottomRight",
            message:"Error",
            description:data.info.message
          });
        }
        setIsLoading(false);
      })
      .catch(error => {
        notification.error({
          placement: 'bottomRight',
          message: 'Error',
          description: error.data
            ? error.data.Message
            : 'Username atau Password salah!',
        });
        setIsLoading(false);
      })
    }).catch(err => {
      return err
    })
  }

  const handleRegister = () => {
    // mengganti route path ke login
    history.push(`/login/`)
  }

  return (
    <div className="content">
      <div className="container">
        <div style={{display: 'flex', alignItems: 'center', height: '100%'}}>
          <Row className="login-row" gutter={[20, 20]} justify='center'>
            <Col span={24}>
              <Row justify='center'>
                <Col>
                    <img className="defaultLogo" alt="defaultLogo" src={urlLogo}/>
                </Col>
              </Row>
              <Row justify='center'>
                <Col lg={8} md={12} className="contentainer">
                  <Card className="app-card" style={{padding: '0px 10px'}}>
                    <Form name="formLogin" form={form} onFinish={values => handleFinish()}>
                      <div className="login-title">Lupa Password</div>
                      <Row>
                        <Col span={24} style={{textAlign: "left",marginBottom:'8px'}}>
                          Email
                        </Col>
                        <Col span={24} style={{ marginBottom: '15px' }}>
                        <Form.Item
                          name='email'
                          style={{marginBottom: '0em'}}
                          rules={[{required: true, message: 'Username Harus diisi!'}]}
                        >
                          <Input
                            prefix={<UserOutlined />}
                            placeholder="Email"
                            className="input-style-login"
                          />
                        </Form.Item>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row>
                            <Col span={10}>
                            </Col>
                            <Col span={14} style={{ textAlign: 'right'}}>
                              <Button
                                htmlType='submit'
                                size="large"
                                className="app-btn primary"
                                style={{backgroundColor:"#00C3D4",fontWeight:"bold",border:"none"}}
                                type="primary"
                                loading={isLoading}
                                >
                                Submit
                              </Button>
                            </Col>
                          </Row>
                        </Col>
                        <Col span={24} style={{marginTop:20}}>
                          <Row style={{padding:0,margin:0}}>
                            <Col style={{padding:0,margin:0}} span={24} style={{ textAlign: 'right'}}>
                              <Link onClick={handleRegister} className="daftarDisini">Login disini</Link>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Form>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
}


export default ForgetPassword
