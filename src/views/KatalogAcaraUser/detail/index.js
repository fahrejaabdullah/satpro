import React, { useEffect, useState } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Button,
  Row,
  Col,
	Spin,
	Popconfirm
} from 'antd'
import { endpoint } from '../../../api/katalogAcaraUser';
import {EditOutlined} from '@ant-design/icons'
import moment from 'moment';

const currentDate = new Date
const text = 'Apakah anda yakin akan mengikuti acara ini ?'
const KatalogAcaraUserDetail = props => {
	const [isLoading, setLoading] = useState(true)
	const [data, setData] = useState({})

	useEffect(() => {
		// set id dan token pada saat page pertama kali di buka
		const body = {
			id: props.location.pathname.split('/')[2],
			token: props.token
		}
		getData(body)
	},[])

	const getData = body => {
		// get data detail katalog acara
		trackPromise(
      endpoint.getAcaraDetail(body).then(res => {
				setData(res.data)
				setLoading(false)
      })
    )
	}

	const handleRegis = () => {
		setLoading(true)
		const body = {
			id: props.location.pathname.split('/')[2],
			token: props.token
		}
		// hit API untuk registrasi user
		endpoint.regisAcara(body).then(res=> {
			setLoading(false)
			if (data.isPembayaran) {
			window.open(res.token, '_blank')
			}
		})
	}

	
	const handleDisable = tanggal => {
		// disable button register jika tanggal acara sudah terlewat
		return moment(tanggal).isBefore(currentDate)
	}


  return(
    <div className='root'>
			<Spin spinning={isLoading}>
				<Row gutter={props.isMobile ? [16, 16] : [4,4]}>
					<Col span={24} style={{marginBottom: '40px'}}>
						<Card 
							className='containCardDetail'
						>
							<Row>
								<Col span={props.isMobile ? 24 : 12}>
									<Card
										className='cardDetail'
										cover={<img alt="example" className='imageCardDetail' src={data.banner} />}
									>
									</Card>
								</Col>
								<Col span={props.isMobile ? 24 : 12}>
									<Row gutter={props.isMobile ? [16, 16] : [0, 8]}>
										<Col span={24}>
											<span className="detailAcaraCard">
												{data.nama}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Deskripsi : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.deskripsi}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Jenis Acara : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.jenis}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Narasumber : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.narasumber}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Hari / Tanggal : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.tanggal}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Jam : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.jam}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Media : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.media}
											</span>
										</Col>
										{data.isPembayaran && (
											<Col span={props.isMobile ? 12 : 6}>
												<span className='descTextAcaraBold'>
													Biaya Pendaftaran : 
												</span>
											</Col>
										)}
										{data.isPembayaran && (
											<Col span={props.isMobile ? 12 : 18}>
												<span className="descTextAcara">
													{data.biayaPendaftaran}
												</span>
											</Col>
										)}
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												Contan Person : 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											<span className="descTextAcara">
												{data.contactPerson}
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 6}>
											<span className='descTextAcaraBold'>
												{data.media === 'Offline' && 'Alamat: '} 
											</span>
										</Col>
										<Col span={props.isMobile ? 12 : 18}>
											{data.media === 'Offline' && (
												<span className="descTextAcara">
													{data.alamat}
												</span>
											)}
										</Col>
									</Row>
								</Col>
								{!props.isMobile &&
									(
										<Col span={18}>
										</Col>
									)
								}
								<Col span={props.isMobile ? 24 : 6} style={{marginTop: '16px'}}>
									{data.isRegistered ? (
										<Button className="isRegisteredButton">
											Sudah Register
										</Button>
									) : (
										handleDisable(data.tanggal) ? (
											<Button 
												disabled
												className="detailButton" 
												icon={<EditOutlined />}
											>
												Daftar
											</Button>
										) : (
											<Popconfirm
												placement="bottomRight"
												title={text}
												onConfirm={() => handleRegis()}
												okText="Yes"
												cancelText="No"
											>
												<Button 
													className="detailButton" 
													icon={<EditOutlined />}
												>
													Daftar
												</Button>
											</Popconfirm>
										)
									)}
								</Col>
							</Row>
						</Card>
					</Col>
				</Row>
			</Spin>
    </div>
  )
}



export default KatalogAcaraUserDetail