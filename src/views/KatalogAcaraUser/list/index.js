import React, { useEffect, useState } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Button,
  Row,
  Col,
	Tag,
	Input,
	Spin,
	Pagination 
} from 'antd'
import { endpoint } from '../../../api/katalogAcaraUser';
import Meta from 'antd/lib/card/Meta';
import {SearchOutlined} from '@ant-design/icons'
import debounce from 'lodash.debounce'
import { UIModalVerif } from '../../../components';

const KatalogAcaraUser = props => {
	const delay = 1000
	let debouncedFn;

	const [dataAcara, setDataAcara] = useState([])
	const [isLoading, setLoading] = useState(true)
	const [pagination, setPagination] = useState({
		current: 1,
		total: 0,
		pageSize: 6
	})
	const [body, setBody] = useState({
		search: '',
		size: 6,
		page: 1

	})
	const [dataUser, setDataUser] = useState({
		isVerif: false,
		nama: '',
		email: '',
		history: props.history
	})

	useEffect(() => {
		// memanggil fungsi getData setiap kali state body berubah
		getData(body)
	},[body])

	const getData = param => {
		// get data katalog acara
		trackPromise(
      endpoint.getAcara(param).then(res => {
				setPagination({
					...pagination,
					total: res.totalElements
				})
			 	setDataAcara(res.content || [])
				setLoading(false)
      })
    )
	}

	const handleDetail = id => {
		const userData = JSON.parse(props.userData)
		if (userData?.verified !== 0) {
			// mengganti route path ke detail acara jika akun user sudah di verifikasi
			props.history.push(`katalog-acara/${id}`)
		}else {
			// menampilkan modal untuk memverifikasi email user
			setDataUser({
				isVerif: userData.verified === 0 ? true : false,
				nama: userData.nama,
				email: userData.email,
				history: props.history
			})
		}
	}

	// mendelay fungsi dengan async agar saat memanggil API tidak terjadi penumpukan request API
	const handleChange = async (info) => {
		// memanggil data API setiap kali user menginputkan data ke kolom pencarian
		setLoading(true)
		const keyword = info.target.value;
		if (!debouncedFn) {
			// delay change value dengan parameter delay yang telah di set di sebelumnya(1000)
			debouncedFn = debounce(async (val) => {
			onChange(val);
		  }, delay);
		}
		await debouncedFn(keyword);
	}

	const onChange = val => {
		// merubah state body dengan value pada parameter
		setBody({
			...body,
			search: val
		})
	}

	const handlePagination = (page, size) => {
		// merubah page berdasarkan parameter
		setPagination({
			...pagination,
			current: page
		})
		setBody({
			...body,
			page: page
		})
	}
  return(
    <div className='root'>
			<Spin spinning={isLoading}>
				<Row gutter={ props.isMobile ? [16,16] : [4,4]}>
					<Col span={24} style={{marginBottom: props.isMobile ? '20px' : '40px' }}>
						<Col span={props.isMobile ? 24 : 8}>
							<Input 
								placeholder='Cari nama acara' 
								style={{borderRadius: '8px', border: '1px solid #00C3D4'}} 
								suffix={<SearchOutlined color='#333333'/>}
								onChange={handleChange}
							/>
						</Col>
					</Col>
					{dataAcara.length > 0 ? (
						dataAcara.map((res, index) => (
							<Col span={8} xs={24} md={8} key={index + 'col' + res.idAcara}>
								<Card
									className='cardContent'
									cover={<img alt="example" src={res.banner} />}
								>
									<Meta
										title={res.jenis}
									>
									</Meta>
									<Row gutter={[0, 8]}>
										<Col span={24}>
											<span className="detailAcara">
												{res.nama}
											</span>
										</Col>
										<Col span={24} style={{minHeight: '80px', maxHeight: '80px', overflow: 'hidden'}}>
											<pre className="deskAcara">
												{res.deskripsi}
											</pre>
										</Col>
										<Col span={12}>
											<Tag color="#202025">
												{res.tanggal}
											</Tag>
										</Col>
										<Col span={12}>
											<Tag color="#202025">
												{res.jam}
											</Tag>
										</Col>
										<Col span={8}>
											<Tag color="#202025">
												{res.contactPerson}
											</Tag>
										</Col>
										<Col span={24} style={{marginTop: '16px'}}>
											<Button className="detailButton" onClick={() => handleDetail(res.idAcara)}>
												Lihat Detail {'>'}
											</Button>
										</Col>
									</Row>
								</Card>
							</Col>
						))
					) : (
						<Col span={24} style={{textAlign: 'center'}}>
							<span className='descTextAcaraBold' style={{fontSize: '32px'}}>
								No Data
							</span>
						</Col>
					)}
					{dataAcara.length > 0 && (
						<Col span={24}
							style={{marginTop: '30px', marginBottom: '30px', textAlign:'center'}}
						>
							<Pagination 
								current={pagination.current} 
								total={pagination.total} 
								pageSize={pagination.pageSize}
								onChange={(page, pageSize) => handlePagination(page, pageSize)}
							/>
						</Col>
					)}
				</Row>
				<UIModalVerif 
					dataUser={dataUser}
				/>
			</Spin>
    </div>
  )
}



export default KatalogAcaraUser