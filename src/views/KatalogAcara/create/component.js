import React, { useEffect, useState } from "react";
import {
  Row, 
  Col, 
  Button, 
  Form,
  Input, 
  Card, 
  DatePicker, 
  Radio,
  Typography,
  Select
} from "antd";
import "../../../global/global.css";
import findName from "../../../functions/findNameFile";
import TextArea from 'antd/lib/input/TextArea';
import UploadComponentValues from "../../../components/uploadComponentValues";
import moment from 'moment';
import { RedStar } from '../../../components';

const {Text} = Typography
const { Option } = Select

const lovMedia = [{
  key: true,
  value: 'Berbayar'
},{
  key: false,
  value: 'Gratis'
}]

function FormKatalog(props) {
  const dataLov = props.data
	const [form] = Form.useForm();
  const [defaultBanner, setDefaultBanner]= useState([])
  const [isOnline, setIsOnline] = useState(props?.initialData?.media === 'Online' ? true : false)
  const [isPembayaran, setIsPembayaran] = useState(props?.initialData?.isPembayaran === undefined ? true : props?.initialData?.isPembayaran)

  useEffect(() => {
    if(props.initialData && props.isEdit){
      // jika isEdit yang di kirimkan parent bernilai true maka akan memaping data
      mappingValueDetail(props.initialData)
      setIsPembayaran(props?.initialData?.isPembayaran === undefined ? true : props?.initialData?.isPembayaran)
      setIsOnline(props?.initialData?.media === 'Online' ? true : false)
    }
  }, [props.initialData])

  const mappingValueDetail = e => {
    // maping data, agar pada saat pertama kali user masuk kedalam page form sudah terisi
    if (e) {
      let body = {
        nama : e.nama,
        alamat : e.alamat,
        deskripsi: e.deskripsi,
        media : e.media,
        tanggal : moment(e.tanggal, 'DD-MM-YYYY HH:mm'),
        narasumber : e.narasumber,
        linkPelaksanaan : e.linkPelaksanaan,
        banner : e.banner,
        contactPerson : e.contactPerson,
        jenis: e.jenis,
        hargaPendaftaran: e.hargaPendaftaran,
        isPembayaran: e.isPembayaran
      }
      form.setFieldsValue(body)
      const fileName = findName(e.banner)
      setDefaultBanner([
        {
          uid: e.idAcara || 'uploadId' + Math.random(10),
          name: fileName,
          status: 'done',
          url: e.banner,
          thumbUrl: e.banner
        },
      ])
      return body
    }
  }

	const onFinish = async(values) => {
    // submit form
    props.onFinish(values);
	}

	const onFinishFailed = error => {
    // menampilkan pesan error jika pengisian form tidak sesuai
    console.log(error)
	}
  const disabledDate = (current) => {
    // disable data yang telah melewati hari ini
    return current && current < moment().endOf('day');
  }

  const handleOnline = e => {
    // normalisasi data online
    let alreadyOnline = false
    if (e === 'Online') {
      alreadyOnline = true
    }
    setIsOnline(alreadyOnline)
  }

  const handledataChange = data => {
    // set data banner yang telah di upload oleh user
    form.setFieldsValue({
      banner: (data?.fileList[0]?.response?.data)
    })
    if (data.file.status === 'done') {
      setDefaultBanner([
        {
          ...data?.file
        }
      ])
    }
  }

  const removeImage = data => {
    // menghapus data image
    if (data.status === 'removed') {
      setDefaultBanner([])
    }
    form.setFieldsValue({
      banner: undefined
    })
  }

  const handlePembayaran = e => {
    // set data pembayaran
    setIsPembayaran(e.target.value)
  }


  const CardHeader = () => {
    return (
      <Card className='bodyCard_style'>
        <Card
          bordered={false}
          style={{minHeight: '300px'}}>
            <Row gutter={[30]}>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Nama Acara <RedStar /></Text>
                <Form.Item name='nama'
                  rules={[{required: true,message: 'Nama Acara harus diisi!' }
                ]}>
                  <Input
                    placeholder={'Masukan Nama Acara'}
                    size="large"
                    className='input_style'
                    maxLength={256}
                  />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Deskripsi Acara <RedStar /></Text>
                <Form.Item name='deskripsi'
                  rules={[{required: true,message: 'Deskripsi harus diisi!' }
                ]}>
                  <TextArea placeholder={'Masukan deskripsi acara'}  size="large" className='input_style' />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Media <RedStar /></Text>
                <Form.Item name='media'
                  rules={[{required: true,message: 'Media harus diisi!' }
                ]}>
                   <Select 
                      style={{  
                        textAlign: 'left' 
                      }}
                      placeholder='Pilih media'
                      onChange={e => handleOnline(e)}
                    >
                      <Option value="Online">Online</Option>
                      <Option value="Offline">Offline</Option>
                    </Select>
                </Form.Item>
              </Col>
              {!isOnline ? (
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Alamat Acara{!isOnline && <RedStar />}</Text>
                  <Form.Item name='alamat'
                    rules={[{required: !isOnline,message: 'Alamat Acara harus diisi!' }
                  ]}>
                    <TextArea placeholder={'Masukan alamat acara'}  size="large" className='input_style' />
                  </Form.Item>
                </Col>
              ):(
                <Col span={props.isMobile ? 24 : 12}>
                  <Text className="form-label">Link Pelaksanaan {isOnline && <RedStar />}</Text>
                  <Form.Item name='linkPelaksanaan'
                    rules={[{required: isOnline,message: 'Link Pelaksanaan harus diisi!' }
                  ]}>
                    <Input placeholder={'Masukan link pelaksaan'}  size="large" className='input_style'/>
                  </Form.Item>
                </Col>
              )}
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Narasumber <RedStar /></Text>
                <Form.Item name='narasumber'
                  rules={[{required: true,message: 'Narasumber harus diisi!' }
                ]}>
                  <Input placeholder={'Masukan nama narasumber'}  size="large" className='input_style'/>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Hari, Tanggal Acara <RedStar /></Text>
                <Form.Item name='tanggal'
                  rules={[{required: true,message: 'Hari, Tanggal Acara  harus diisi!' }
                ]}>
                  <DatePicker 
                    placeholder={'dd/mm/yyyy, --:--'} 
                    showTime={true}
                    className='input_style'
                    format={'DD-MM-YYYY HH:mm'}
                    disabledDate={disabledDate}
                  />
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Contact Person <RedStar /></Text>
                <Form.Item name='contactPerson'
                  rules={[{required: true,message: 'Contact Person harus diisi!' }
                ]}>
                  <Input placeholder={'Masukan nama & telepon contact person'}  size="large" className='input_style'/>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Jenis Acara <RedStar /></Text>
                <Form.Item name='jenis'
                  rules={[{required: true,message: 'Jenis Acara harus diisi!' }
                ]}>
                  <Select 
                    style={{  
                      textAlign: 'left' 
                    }}
                    placeholder='Pilih jenis acara'
                  >
                    {dataLov.map((res, index) => (
                      <Option value={res.jenisAcara} key={res.jenisAcara + index}>{res.jenisAcara}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Metode Pembayaran <RedStar /></Text>
                <Form.Item  name='isPembayaran'
                  rules={[{required: true,message: 'Media harus diisi!' }
                ]}>
                  <Radio.Group>
                    <Row gutter={[0,8]}>
                      {lovMedia.map((res) => (
                        <Col span={12} key={res.key}>
                          <Radio 
                            key={res.key} 
                            value={res.key}
                            onChange={e => handlePembayaran(e)}
                          >
                            {res.value}
                          </Radio>
                        </Col>
                      ))}
                    </Row>
                  </Radio.Group>
                </Form.Item>
              </Col>
              {isPembayaran ? (
                <Col span={props.isMobile ? 24 : 12}>
                  <Text className="form-label">Uang pembayaran <RedStar /></Text>
                  <Form.Item name='hargaPendaftaran'
                    rules={[{required: true,message: 'Uang pembayaran harus diisi!' }
                  ]}>
                    <Input placeholder={'Masukan uang pembayaran'} size="large" className='input_style'/>
                  </Form.Item>
                </Col>
              ):(
                <Col span={props.isMobile ? 24 : 12}>
                  
                </Col>
              ) }
              <Col span={props.isMobile ? 24 : 12} >
                <Text className="form-label">Cover <RedStar /></Text>
                <Form.Item name='banner'
                  rules={[{required: true,message: 'File Cover harus di-upload!' }
                ]}>
                  <UploadComponentValues
                    pathName='banner'
                    urlAPI={'rest/admin/secured/acara/upload'}
                    token={props.token}
                    name={'thumbailPath'}
                    placeholder='Upload Foto (max:1 file)'
                    accept=".jpg,.png,.jpeg"
                    onChange={handledataChange}
                    defaultBanner={defaultBanner}
                    removeImage={removeImage}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={[20]}>
              <Col span={24}>
                <Form.Item
                  style={{
                    textAlign: 'right',
                    marginBottom: '0px'
                  }}
                >
                  <Button
                    type="primary"
                    htmlType='submit'
                    className='blueButton'
                    size={'small'}
                  >
                    Tambah
                  </Button>
                </Form.Item>
              </Col>
            </Row>
        </Card>
      </Card>
    )
  }

	return (
      <Form
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
        initialValues={{ isPembayaran: isPembayaran}}
      >
        <Row gutter={[30,30]}>
          <Col span={24}>
            <CardHeader/>
          </Col>
          {/* <pre>
            {JSON.stringify(listDataFile)}
          </pre> */}
        </Row>
    </Form>
	)
}

export const UIFormKatalog = FormKatalog;
