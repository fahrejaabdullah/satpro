import React, {Component} from 'react'
import '../../../global/global.css'
import { services } from '../../../api/katalogAcara'
import { trackPromise } from 'react-promise-tracker';
import {
  Spin,
  Typography,
} from 'antd'
import { UIFormKatalog } from './component';
import moment from 'moment';

const { Text } = Typography
class KatalogCreate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lovJenisPekerjaan: [],
      loading: false,
      isEdit: false,
      initialData: null,
      eventId: null
    }
  }

  componentDidMount() {
    // get data initial saat pertama kali page dibuka
    const id = this.props.location.pathname.split('/')[2]
    const type = this.props.location.pathname.split('/')[1]
    this.getJenisAcara(this.props.token)
    if(type == 'katalog-edit' && id){
      const body = {
        token: this.props.token,
        id: id
      }
      this.setState({
        eventId: id,
        loading: true,
        isEdit: true
      }, () => this.getDataId(body))
    }
  }

  getJenisAcara = (token) => {
    // get data jenis acara
    trackPromise(
      services.getJenisAcara(token).then(res => {
        this.setState({
          lovJenisPekerjaan: res.data,
          loading: false
        })
      })
    )
  }

  getDataId = param => {
    // get data katalog sesuai dengan yang dipilih user
		trackPromise(
      services.getDataDetail(param).then(res => {
        let dataTemp = {
          ...res.data.data
        }
        this.setState({
          initialData: dataTemp,
          loading: false
        })
      })
    )
  }

  editHeaderFooter = (data) => {
    let body = {
      ...data,
      tanggal: moment(data.tanggal).format('YYYY-MM-DD HH:mm'),
      usersId: this.props.userId
    }
    if(this.state.isEdit){
      // mengirimkan data yang telah di edit oleh user
      body.eventId = this.state.eventId
      body.banner = 
      trackPromise(
        services.edit(this.props.token, this.state.eventId, body).then((res) => {
          if(res == 'success'){
            this.props.history.push('/katalog-acara-admin')
          }
        })
      )
    }else{
      // mengirimkan data baru yang telah di isi oleh user
      trackPromise(
        services.create(this.props.token, body).then((res) => {
          if(res == 'success'){
            this.props.history.push('/katalog-acara-admin')
          }
          return true
        })
      )
    }
  }

  render(){
    const { lovJenisPekerjaan, loading, isEdit, initialData } = this.state
    return (
      <div className='root'>
        <Spin spinning={loading}>
          <UIFormKatalog 
            data={lovJenisPekerjaan}
            onFinish={(e)=>this.editHeaderFooter(e)}
            getLov={e => this.getData(e)}
            token = {this.props.token}
            isEdit = {isEdit}
            initialData = {initialData}
            isMobile = {this.props.isMobile}
          />
        </Spin>
      </div>
    )
  }
}


export default KatalogCreate
