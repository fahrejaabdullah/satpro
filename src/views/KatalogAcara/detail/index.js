import React, { Component } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Select,
  Row,
  Col,
  Typography,
  Button
} from 'antd'
import { UISearch, UItableNew } from '../../../components'
import { endpoint } from '../../../api/apiDataTable';
import { services } from '../../../api/katalogAcara';

const { Text } = Typography
const initialOrderBy = 'u.nama,desc'
const { Option } = Select
class KatalogDetail extends Component {
	constructor(props) {
		super(props)
		this.state = {
		initial: '',
		orderBy: initialOrderBy,
		size: 10,
		page: 1,
		search: '',
		visibleCreate: false,
		initalData: {},
		dataId: undefined,
		isEdit: false,
		dataProfil: {
      data: [],
      totalData: 0
    },
		dataLOV: [],
    loadingSubmit: false,
    loadingExport: false,
    apiName: 'getDataPeserta',
		id: props.location.pathname.split('/')[2],
		}
	}

	getData = param => {
    // get data peserta
		trackPromise(
      endpoint.getDataPeserta(param).then(res => {
        this.setState({
          dataProfil: res
        })
      })
    )
	}

	handleSearch = key => {
    // memanggil API setiap kali user melakukan pencarian
    const {orderBy, size } = this.state
    this.setState({
      search: key,
      page: 1
    })
    const body = {
      token: this.props.token,
      sort: orderBy,
      size: size,
      page: 1,
      search: key,
      idAcara: this.state.id
    }
    this.getData(body)
  }

  handleExport = () => {
    trackPromise(
      services.exportDataPerserta(this.props.token, this.state.id).then(res => {
        this.handleBlob(res.data)
      })
    )
  }

  handleBlob = data => {
    // download data list peserta
    var blob = new Blob([data], {type: 'application/vnd.ms-excel'});
    var downloadUrl = URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = downloadUrl;
    a.download = "datapartisipasi.xls";
    document.body.appendChild(a);
    a.click();
  }

	render() {
		const {page, dataProfil, apiName } = this.state
    const columnsTable = [
      {
        title: 'Nama',
        dataIndex: 'nama',
        key: 'u.nama',
        sorter: true,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        sorter: true,
      },
      {
        title: 'No Telp',
        dataIndex: 'noTelp',
        key: 'u.no_telp',
        sorter: true,
      },
      {
        title: 'Tanggal Lahir',
        dataIndex: 'tanggalLahir',
        key: 'u.tanggal_lahir',
        sorter: true,
      }
    ]

		return(
			<div className='root'>
				<Card className='bodyCard_style'>
					<Row gutter={this.props.isMobile ? [16,16] : [16,0]} justify="space-between">
            <Col span={8} xs={24} md={8}>
              <UISearch placeholder='Cari Nama Peserta...' handleSearch={key => this.handleSearch(key)} />
            </Col>
            <Col span={12} xs={24} md={16} style={{textAlign: !this.props.isMobile && 'end'}}>
              <Button onClick={()=> this.handleExport()}className='addButton'>Export</Button>
            </Col>
          </Row>
					<Card 
            bordered={false}
            style={{minHeight: '300px'}}>
            <UItableNew
              scroll={{x: 800}}
              apiName={apiName}
              columns={columnsTable}
              token={this.props.token}
              withRows={false}
              page={page}
              totalData={dataProfil.totalData}
              data={dataProfil.data}
              sortDefault=''
              extendParam={{idAcara: this.state.id}}
              isExtendParam={true}
            />
          </Card>
				</Card>
			</div>
		)
	}
}


export default KatalogDetail