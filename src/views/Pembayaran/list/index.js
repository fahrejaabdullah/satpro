import React, { Component } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Row,
  Col,
  Typography,
  Button
} from 'antd'
import { UISearch, UItableNew } from '../../../components'
import { endpoint } from '../../../api/apiDataTable';
import {services} from '../../../api/pembayaran'

const { Text } = Typography
const initialOrderBy = 'u.nama,desc'

class Pembayaran extends Component {
	constructor(props) {
		super(props)
		this.state = {
		initial: '',
		orderBy: initialOrderBy,
		size: 10,
		page: 1,
		search: '',
		visibleCreate: false,
		initalData: {},
		dataId: undefined,
		isEdit: false,
		dataProfil: {
      data: [],
      totalData: 0
    },
		dataLOV: [],
    loadingSubmit: false,
    loadingExport: false,
    apiName: 'getDataListPembayaran'
		}
	}

	getDataListPembayaran = param => {
    // get data list pembayaran
		trackPromise(
      endpoint.getDataListPembayaran(param).then(res => {
        this.setState({
          dataProfil: res
        })
      })
    )
	}

	handleSearch = key => {
    // memanggil API setiap kali user melakukan pencarian
    const {orderBy, size, apiName } = this.state
    this.setState({
      search: key,
      page: 1
    })
    const body = {
      token: this.props.token,
      sort: orderBy,
      size: size,
      page: 1,
      search: key,
    }
    this[apiName](body)
  }

  handleExport = () => {
    trackPromise(
      services.exportData(this.props.token).then(res => {
        this.handleBlob(res.data)
      })
    )
  }

  handleBlob = data => {
    // download data list pembayaran
    var blob = new Blob([data], {type: 'application/vnd.ms-excel'});
    var downloadUrl = URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = downloadUrl;
    a.download = "datapembayaran.xls";
    document.body.appendChild(a);
    a.click();
  }

	render() {
		const {page, dataProfil, apiName } = this.state
    const columnsTable = [
      {
        title: 'Nama Perserta',
        dataIndex: 'nama',
        key: 'u.nama',
        sorter: true,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'u.email',
        sorter: true,
      },
      {
        title: 'No. Telp',
        dataIndex: 'noTelp',
        key: 'u.no_telp',
        sorter: true,
      },
      {
        title: 'Nama Acara',
        dataIndex: 'namaAcara',
        key: 'a.nama',
        sorter: true,
      },
      {
        title: 'Tanggal',
        dataIndex: 'tanggal',
        key: 'a.tanggal',
        sorter: true,
      },
      {
        title: 'Nominal',
        dataIndex: 'nominal',
        key: 'a.harga_pendaftaran',
        sorter: true,
      },
      {
        title: 'Status Pembayaran',
        dataIndex: 'pembayaranStatus',
        key: 'pembayaran_status',
        sorter: true,
      },
    ]

		return(
			<div className='root'>
				<Card className='bodyCard_style'>
					<Row gutter={this.props.isMobile ? [16,16] : [16,0]} justify="space-between">
            <Col span={8} xs={24} md={8}>
              
              <UISearch placeholder='Cari Nama, status...' handleSearch={key => this.handleSearch(key)} />
            </Col>
            <Col span={16} xs={24} md={16} style={{textAlign: !this.props.isMobile && 'end'}}>
              <Button onClick={()=> this.handleExport()} style={{marginRight: '10px'}} className='addButton'>Export</Button>
            </Col>
          </Row>
					<Card 
            bordered={false}
            style={{minHeight: '300px'}}>
            <UItableNew
              scroll={{x: 800}}
              apiName={apiName}
              columns={columnsTable}
              token={this.props.token}
              withRows={false}
              page={page}
              totalData={dataProfil.totalData}
              data={dataProfil.data}
              sortDefault=''
            />
          </Card>
				</Card>
			</div>
		)
	}
}


export default Pembayaran