import React, { useEffect, useState } from "react";
import {
  Row, 
  Col, 
  Button, 
  Form,
  Input, 
  Card, 
  Typography,
  Select,
  AutoComplete
} from "antd";
import "../../../global/global.css";
import moment from 'moment';
import { RedStar } from '../../../components';
import { services } from '../../../api/doorprize'
import { trackPromise } from 'react-promise-tracker';

const {Text} = Typography
const { Option } = Select

const lovMedia = [{
  key: true,
  value: 'Berbayar'
},{
  key: false,
  value: 'Gratis'
}]

function FormDoorprize(props) {
  const [lovPemenang,setDataLovPemenang] = useState(props.lovPemenang); 
  const lovStatus = props.lovStatus; 
  const lovHadiah = props.lovHadiah;
  const [lovAcara, setDataLovAcara] = useState(props.lovAcara)
  const [selectedAcara, setSelectedAcara] = useState(null)
  const [selectedPemenang, setSelectedPemenang] = useState(null)

	const [form] = Form.useForm();
  const [loadingSubmit, setLoadingSubmit] = useState(false)
  const [defaultBanner, setDefaultBanner]= useState([])
  const [isOnline, setIsOnline] = useState(false)
  const [isOpenNamaAcara, setIsOpenNamaAcara] = useState(false)
  const [isOpenNamaPemenang, setIsOpenNamaPemenang] = useState(false)

  useEffect(() => {
    if(props.initialData && props.isEdit){
      mappingValueDetail(props.initialData)
    }
  }, [props.initialData])

  const getPemenang = (token,param) => {
    trackPromise(
      services.getPemenang(token,param).then(res => {
        setDataLovPemenang(res)
      })
    )
  }

  useEffect(() => {
    setDataLovAcara(props.lovAcara)
  }, [props.lovAcara])
  

  const mappingValueDetail = e => {
    let body = {
      namaAcara: e.namaAcara,
      namaPemenang: e.namaPemenang,
      noResi: e.noResi,
      status: e.status,
      hadiah: e.jenis,
    }
    form.setFieldsValue(body)
    setSelectedPemenang(e.email)
    setSelectedAcara(e.idAcara)
    return body
  }

	const onFinish = async(values) => {
    var _values = {
      idAcara:selectedAcara,
      email:selectedPemenang,
      noResi:values.noResi,
      jenis:values.hadiah,
      status:values.status
    };
    props.onFinish(_values);
	}

	const onFinishFailed = error => {
	}
  const disabledDate = (current) => {
    return current && current < moment().endOf('day');
  }

  const handleOnline = e => {
    let alreadyOnline = false
    if (e === 'Online') {
      alreadyOnline = true
    }
    setIsOnline(alreadyOnline)
  }

  const onSelectAcara = e => {
    var result = lovAcara.filter(obj => {
      return obj.id === e
    })
    
    let body = {
      namaAcara : result[0].label
    }
    // setDataLovPemenang([]);
    var token= sessionStorage.getItem('token') ? sessionStorage.getItem('token') : localStorage.getItem('token') ? localStorage.getItem('token') : null;
    getPemenang(token,e);
    form.setFieldsValue(body);
    setSelectedAcara(result[0].id);
    // namaAcara
  }

  const onSelectPemenang = e => {
    var result = lovPemenang.filter(obj => {
      return obj.id === e
    })
    
    let body = {
      namaPemenang : result[0].label
    }
    form.setFieldsValue(body);
    setSelectedPemenang(result[0].id)
    // namaAcara
  }

  const handledataChange = data => {
    form.setFieldsValue({
      banner: (data?.fileList[0]?.response?.data)
    })
  }

  const CardHeader = () => {
    return (
      <Card className='bodyCard_style'>
        <Card
          bordered={false}
          style={{minHeight: '300px'}}>
            <Row gutter={[30]}>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Nama Acara <RedStar /></Text>
                <Form.Item name='namaAcara'
                  rules={[{required: true,message: 'Nama Acara harus diisi!' }
                ]}>
                  <AutoComplete
                    onSelect={onSelectAcara}
                    options={lovAcara}
                    filterOption={(inputValue, option) =>
                      option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                    }
                    placeholder={'Nama Acara'}
                    allowClear
                    // autoFocus
                    // defaultOpen={isOpenNamaAcara}
                  >
                    <Input maxLength={256} />
                  </AutoComplete>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12} >
                <Text className="form-label">Hadiah <RedStar /></Text>
                <Form.Item name='hadiah'
                  rules={[{required: true,message: 'Hadiah harus dipilih!' }
                ]}>
                  <Select 
                    style={{  
                      textAlign: 'left' 
                    }}
                    placeholder='Pilih hadiah'
                  >
                    {lovHadiah.map((res, index) => (
                      <Option value={res.jenisHadiah} key={res.jenisHadiah}>{res.jenisHadiah}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Nama Pemenang <RedStar /></Text>
                <Form.Item name='namaPemenang'
                  rules={[{required: true,message: 'Nama Pemenang Harus diisi!' }
                ]}>
                  <AutoComplete
                    // onChange={changeAutoComplete}
                    onSelect={onSelectPemenang}
                    options={lovPemenang}
                    filterOption={(inputValue, option) =>
                      option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                    }
                    placeholder={'Nama Pemenang'}
                    allowClear
                    // autoFocus
                    // defaultOpen={isOpenNamaPemenang}
                  >
                    <Input maxLength={256} />
                  </AutoComplete>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">No Resi <RedStar /></Text>
                <Form.Item name='noResi'
                  rules={[{required: true,message: 'No Resi harus diisi!' }
                ]}>
                  <Input placeholder={'Masukan no resi'}  size="large" className='input_style'/>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
                <Text className="form-label">Status <RedStar /></Text>
                <Form.Item name='status'
                  rules={[{required: true,message: 'Status harus dipilih!' }
                ]}>
                  <Select 
                    style={{  
                      textAlign: 'left' 
                    }}
                    placeholder='Pilih status'
                  >
                    {lovStatus.map((res, index) => (
                      <Option value={res.status} key={res.status}>{res.status}</Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={props.isMobile ? 24 : 12}>
              </Col>
            </Row>
            <Row gutter={[20]}>
              <Col span={24}>
                <Form.Item
                  style={{
                    textAlign: 'right',
                    marginBottom: '0px'
                  }}
                >
                  <Button
                    type="primary"
                    htmlType='submit'
                    className='blueButton'
                    size={'small'}
                    loading={loadingSubmit}
                  >
                    Tambah
                  </Button>
                </Form.Item>
              </Col>
            </Row>
        </Card>
      </Card>
    )
  }

	return (
      <Form
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
      >
        <Row gutter={[30,30]}>
          <Col span={24}>
            <CardHeader/>
          </Col>
          {/* <pre>
            {JSON.stringify(listDataFile)}
          </pre> */}
        </Row>
    </Form>
	)
}

export const UIFormKatalog = FormDoorprize;
