import React, {Component} from 'react'
import '../../../global/global.css'
import { services } from '../../../api/doorprize'
import { trackPromise } from 'react-promise-tracker';
import {
  Spin,
  Typography,
} from 'antd'
import { UIFormKatalog } from './component';

const { Text } = Typography
class DoorprizeCreate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lovAcara: [],
      lovHadiah: [],
      lovStatus: [],
      loading: false,
      isEdit: false,
      initialData: {},
      eventId: null
    }
  }

  componentDidMount() {
    const id = this.props.location.pathname.split('/')[2]
    const type = this.props.location.pathname.split('/')[1]
    this.getJenisAcara(this.props.token)
    this.getHadiah(this.props.token)
    this.getStatus(this.props.token)
    if(type == 'doorprize-edit' && id){
      const body = {
        token: this.props.token,
        id: id
      }
      this.setState({
        eventId: id,
        loading: true,
        isEdit: true
      }, () => this.getDataId(body))
    }
  }

  getJenisAcara = (token) => {
    trackPromise(
      services.getAcara(token).then(res => {
        this.setState({
            lovAcara: res,
        })
      })
    )
  }

  getHadiah = (token) => {
    trackPromise(
      services.getHadiah(token).then(res => {
        this.setState({
            lovHadiah: res.data,
        })
      })
    )
  }

  

  getStatus = (token) => {
    trackPromise(
      services.getStatus(token).then(res => {
        this.setState({
          lovStatus: res.data,
        })
      })
    )
  }

  getDataId = param => {
		trackPromise(
      services.getDataDetail(param).then(res => {
        let dataTemp = {
          ...res.data.data
        }
        this.setState({
          initialData: dataTemp,
          loading: false
        })
      })
    )
  }

  editHeaderFooter = (data) => {
    let body = {
      ...data,
      usersId: this.props.userId
    }
    if(this.state.isEdit){
      body.eventId = this.state.eventId
      body.banner = 
      trackPromise(
        services.edit(this.props.token, this.state.eventId, body).then((res) => {
          if(res == 'success'){
            this.props.history.push('/hadiah-doorprize')
          }
        })
      )
    }else{
      trackPromise(
        services.create(this.props.token, body).then((res) => {
          if(res == 'success'){
            this.props.history.push('/hadiah-doorprize')
          }
          return true
        })
      )
    }
  }

  render(){
    const {lovHadiah, lovStatus, lovAcara, loading, isEdit, initialData } = this.state
    return (
      <div className='root'>
        <Spin spinning={loading}>
          <UIFormKatalog 
            lovAcara={lovAcara}
            lovHadiah={lovHadiah}
            isMobile={this.props.isMobile}
            lovStatus={lovStatus}
            onFinish={(e)=>this.editHeaderFooter(e)}
            getLov={e => this.getData(e)}
            token = {this.props.token}
            isEdit = {isEdit}
            initialData = {initialData}
          />
        </Spin>
      </div>
    )
  }
}


export default DoorprizeCreate
