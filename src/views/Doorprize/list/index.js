import React, { Component } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Button,
  Row,
  Col,
  Popconfirm
} from 'antd'
import { UISearch, UItableNew } from '../../../components'
import { endpoint } from '../../../api/apiDataTable';
import { services } from '../../../api/doorprize'
import { DeleteOutlined, EditOutlined} from '@ant-design/icons';

const initialOrderBy = ''
class Doorprize extends Component {
	constructor(props) {
		super(props)
		this.state = {
		initial: '',
		orderBy: initialOrderBy,
		size: 10,
		page: 1,
		search: '',
		visibleCreate: false,
		initalData: {},
		dataId: undefined,
		isEdit: false,
		dataProfil: {
      data: [],
      totalData: 0
    },
		dataLOV: [],
    loadingSubmit: false,
    loadingExport: false,
    apiName: 'getDataListDoorprize'
		}
	}

	getDataListDoorprize = param => {
    // get data list doorprize
		trackPromise(
      endpoint.getDataListDoorprize(param).then(res => {
        this.setState({
          dataProfil: res
        })
      })
    )
	}

	handleSearch = key => {
    // get data saat user melakukan pencarian
    const {orderBy, size, apiName } = this.state
    this.setState({
      search: key,
      page: 1
    })
    const body = {
      token: this.props.token,
      sort: orderBy,
      size: size,
      page: 1,
      search: key,
    }
    this[apiName](body)
  }

	handleRedirectAdd = () => {
    // mengganti route path ke add doorprize
    this.props.history.push(`doorprize-add`)
	}
	handleRedirectEdit = id => {
    // mengganti route path ke edit doorprize
    this.props.history.push(`doorprize-edit/${id}`)
	}

	handleReject = id => {
    // menghapus data doorpize sesuai dengan yang di pilih user
		const body = {
      token: this.props.token,
      sort: this.state.orderBy,
      size: this.state.size,
      page: 1,
      search: this.state.search,
    }
		trackPromise(
      services.deleteData(this.props.token, id).then(res => {
        if (res === 'success') {
          this.setState({
            search: this.state.search || '',
            page: 1,
            orderBy: this.state.orderBy || initialOrderBy,
            visibleCreate: false
          }, () => {
            this[this.state.apiName](body)
          })
        }
      })
    )
	}

  handleExport = () => {
    // export data doorprize
    trackPromise(
      services.exportData(this.props.token).then(res => {
        this.handleBlob(res.data)
      })
    )
  }

  handleBlob = data => {
    // mendownload file yang dikirim kan oleh BE
    var blob = new Blob([data], {type: 'application/vnd.ms-excel'});
    var downloadUrl = URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = downloadUrl;
    a.download = "datadoorprize.xls";
    document.body.appendChild(a);
    a.click();
  }

	render() {

		const {page, dataProfil, apiName } = this.state
    const text = 'Apakah Anda yakin akan menghapus data ini?'
    const columnsTable = [
      {
        title: 'Nama Acara',
        dataIndex: 'namaAcara',
        key: 'a.nama',
        sorter: true,
      },
      {
        title: 'No Resi',
        dataIndex: 'noResi',
        key: 'u.nama',
        sorter: true,
      },
      {
        title: 'Jenis',
        dataIndex: 'jenis',
        key: 'no_resi',
        sorter: true,
      },
      {
        title: 'Nama Pemenang',
        dataIndex: 'namaPemenang',
        key: 'u.nama',
        sorter: true,
      },
      {
        title: 'Status',
        dataIndex: 'status',
        key: 'status',
        sorter: true,
      },
      {
        title: 'Aksi',
        dataIndex: '',
        align: 'center',
        key: '',
        render: (data) => <>
          <Button
            onClick={() => this.handleRedirectEdit(data.idDoorprize)}
            type='link'
            style={{ color: '#00C3D4', cursor: 'pointer', fontWeight: 600 }}
          >
            <EditOutlined style={{fontSize: 20}} />
          </Button>
          <Popconfirm
            placement="bottomRight"
            title={text}
            onConfirm={() => this.handleReject(data.idDoorprize)}
            okText="Yes"
            cancelText="No"
          >
            <Button
              type='link'
              style={{ color: '#00C3D4', cursor: 'pointer', fontWeight: 600 }}
            >
              <DeleteOutlined style={{fontSize: 20}} />
            </Button>
          </Popconfirm>
        </>
      },
    ]

		return(
			<div className='root'>
				<Card className='bodyCard_style'>
					<Row gutter={this.props.isMobile ? [16,16] : [16,0]} justify="space-between">
            <Col span={8} xs={24} md={8}>
              <UISearch placeholder='Nama Acara, narasumber...' handleSearch={key => this.handleSearch(key)} />
            </Col>
            <Col span={16} xs={24} md={16} style={{textAlign: !this.props.isMobile && 'end'}}>
              <Button onClick={()=> this.handleExport()} style={{marginRight: '10px'}} className='addButton'>Export</Button>
              <Button onClick={() => this.handleRedirectAdd()} className='addButton'>+ Tambah Doorprize</Button>
            </Col>
          </Row>
					<Card 
            bordered={false}
            style={{minHeight: '300px'}}>
            <UItableNew
              scroll={{x: 800}}
              apiName={apiName}
              columns={columnsTable}
              token={this.props.token}
              withRows={false}
              page={page}
              data={dataProfil.data}
              totalData={dataProfil.totalData}
              sortDefault=''
            />
          </Card>
				</Card>
			</div>
		)
	}
}


export default Doorprize