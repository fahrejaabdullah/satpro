import { Card, Col, Row, Spin, DatePicker } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useState, useEffect } from 'react';
import masterApi from '../../api/dashboardAdmin';
import {Bar, Pie} from 'react-chartjs-2'
import  './index.scss';
import moment from 'moment';
import {Chart} from 'chart.js'
import ChartDataLabels from 'chartjs-plugin-datalabels';

// register plugin chart data label
Chart.register(ChartDataLabels)

const color = [
  '#C19B3A',
  '#A9A7A3',
  '#B25EC7',
  '#4BB8D3',
  '#329A7E',
  '#9BB06E'
]
export default function Dashboard(props) {
  const dateNow = new Date()
  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    plugins: {
      datalabels: {
        display: false,
      }
    }
  };

  const [data, setData] = useState({
    labels: [],
    datasets: []
  });
  const [filterDate, setDate] = useState({
    month: dateNow.getMonth() + 1,
    year: dateNow.getFullYear()
  })
  const [labels, setLabels] = useState([])
  const [dataset, setDataset] = useState([])
  const [labelsPie, setLabelPie] = useState([])
  const [datasetPie, setdatasetPie] = useState([])
  const [totalNumber, setCountNumber] = useState(0)
  const [isLoading, setIsLoading] = useState(true);
  const [dataPie, setDataPie] = useState(
    {
      labels: [],
      datasets: [
        {
          label: '# of Votes',
          data: [],
          backgroundColor: color,
          borderColor: color,
        }
      ]
    }
  )

  useEffect(() => {
    // set data cuurent date
    if (props.token) {
      loadData(filterDate);
    }
  }, [filterDate, props.token]);

  useEffect(() => {
    // set data bar chart
    setData({
      labels: labels,
      datasets: dataset
    });
   
  }, [labels, dataset])

  useEffect(() => {
    // set data pie chart
    setDataPie({
      labels: labelsPie,
      datasets: [
        {
          label: '# of Votes',
          data: datasetPie,
          backgroundColor: color,
          borderColor: color,
        }
      ]
    })
  }, [labelsPie, datasetPie])

  Object.size = function(obj) {
    var size = 0,
      key;
    for (key in obj) {
      if (obj.hasOwnProperty(key)) size++;
    }
    return size;
  };

  const onChange = (date, dateString) => {
    // memanggil API saat filter bulan di rubah
    setDate({
      month: moment(date).format('MM'),
      year: moment(date).format('YYYY')
    })
    setIsLoading(true)
  }
  
  const loadData = async (date) => {
    masterApi.getChartAcaraNow(props.token, date).then(res => {
      console.log(res)
      // normalisasi data weekly chart
      const dataTemp = res?.grafikMingguan[0]
      const lenghtObj = Object.size(res?.grafikMingguan[0])
      let tempLabel = Object.keys(res?.grafikMingguan[0])
      tempLabel.sort(function(a, b) {
        var textA = a.toUpperCase();
        var textB = b.toUpperCase();
        if ( textA < textB ) {
          return -1
        }
        if (textA > textB) {
          return 1
        }
        return 0
      });
      setLabels(tempLabel)

      let dataObjTemp = {}
      let datasetTemp = []
      for (let i = 0; i < lenghtObj; i++) {
        dataTemp[tempLabel[i]].map(resData => {
          if (resData.jenisAcara in dataObjTemp === false) {
            dataObjTemp = {
              ...dataObjTemp,
              [resData.jenisAcara]: [resData.jumlahPartisipasi]
            }
          }else {
            dataObjTemp[resData.jenisAcara].push(resData.jumlahPartisipasi)
          }
        })
        
        datasetTemp.push({
          label: Object.keys(dataObjTemp)[i],
          data: dataObjTemp[Object.keys(dataObjTemp)[i]],
          borderColor: color[i],
          backgroundColor: color[i],
        })
      }
      setDataset(datasetTemp)

      // normalisasi all pie chart
      const dataTempPie = res?.grafik
      let dataTempLabelsPie = []
      let dataTempdataPie = []
      let countNumber = 0
      dataTempPie.map(resPie=> {
        dataTempLabelsPie.push(resPie.jenisAcara)
        dataTempdataPie.push(resPie.intPartisipasi)
        countNumber = countNumber + resPie.intPartisipasi
      })
      setCountNumber(countNumber)
      setLabelPie(dataTempLabelsPie)
      setdatasetPie(dataTempdataPie)
      setIsLoading(false)
    })
  }

  return (
    <div className="root">
      <Spin spinning={isLoading}>
        <Row gutter={[30, 30]}>
          <Col span={24}>
            <Card className="app-card full-height">
              <Row gutter={[72,30]}>
                <Col span={24} style={{textAlign: 'center', margin: 10}}>
                  <Title className="chart-title" style={{margin: '10px'}}>Acara yang paling banyak di ikuti di Bulan ini</Title>
                </Col>
                <Col span={24} style={{textAlign: 'center'}}>
                  <DatePicker onChange={onChange} picker="month" format={'MMMM YYYY'} defaultValue={moment(dateNow, 'MMMM YYYY')} />
                </Col>
                <Col span={24} lg={24} md={24} xs={24} style={{textAlign: 'center'}}>
                  <Bar data={data} options={options} width={props.isMobile ? 150 : 600}/>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
        <Row gutter={[30, 30]}>
          <Col span={24}>
            <Card className="app-card full-height">
              <Row gutter={!props.isMobile && [72,30]}>
                <Col span={24} style={{textAlign: 'center', margin: 10}}>
                  <Title className="chart-title" style={{margin: '10px'}}>Acara yang paling banyak di ikuti selama ini</Title>
                </Col>
                <Col span={24} lg={24} md={24} xs={24} style={{textAlign: 'center', textAlign: '-webkit-center'}}>
                  <div style={{maxWidth: !props.isMobile && '40%'}}>
                    <Pie 
                      data={dataPie} 
                      options={{
                        plugins: {
                          datalabels: {
                            color: 'white',
                            display: true,
                            formatter: function(value, context) {
                              if (value !== 0) {
                                return Math.floor((value / totalNumber) * 100) + '%';
                              }else {
                                return ''
                              }
                            }
                          },
                          aspectRatio: 5 / 3,
                        }
                      }}
                    />
                  </div>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </Spin>
    </div>
  )
}
