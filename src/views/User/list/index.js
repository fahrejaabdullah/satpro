import React, { Component } from 'react'
import '../../../global/global.css'

import { trackPromise } from 'react-promise-tracker';
import {
  Card,
  Button,
  Row,
  Col,
  Typography,
  Popconfirm
} from 'antd'
import { UISearch, UItableNew } from '../../../components'
import { endpoint } from '../../../api/apiDataTable';
import { services } from '../../../api/user'
import { DeleteOutlined, EditOutlined, EyeFilled} from '@ant-design/icons';

const { Text } = Typography
const initialOrderBy = ''

class User extends Component {
	constructor(props) {
		super(props)
		this.state = {
		initial: '',
		orderBy: initialOrderBy,
		size: 10,
		page: 1,
		search: '',
		visibleCreate: false,
		initalData: {},
		dataId: undefined,
		isEdit: false,
		dataProfil: {
      data: [],
      totalData: 0
    },
		dataLOV: [],
    loadingSubmit: false,
    loadingExport: false,
    apiName: 'getDataListUser'
		}
	}

	getDataListUser = param => {
    // get data list user
		trackPromise(
      endpoint.getDataListUser(param).then(res => {
        this.setState({
          dataProfil: res
        })
      })
    )
	}

	handleSearch = key => {
    // memanggil API setiap kali user melakukan pencarian
    const {orderBy, size, apiName } = this.state
    this.setState({
      search: key,
      page: 1
    })
    const body = {
      token: this.props.token,
      sort: orderBy,
      size: size,
      page: 1,
      search: key,
    }
    this[apiName](body)
  }

	handleRedirectAdd = () => {
    // mengganti route path dengan user add
    this.props.history.push(`user-add`)
	}
	handleRedirectEdit = id => {
    // mengganti route path dengan user edit
    this.props.history.push(`user-edit/${id}`)
	}

	handleReject = id => {
		const body = {
      token: this.props.token,
      sort: this.state.orderBy,
      size: this.state.size,
      page: 1,
      search: this.state.search,
    }
    // menghapus data yang di pilih oleh user
		trackPromise(
      services.deleteData(this.props.token, id).then(res => {
        if (res === 'success') {
          this.setState({
            search: this.state.search || '',
            page: 1,
            orderBy: this.state.orderBy || initialOrderBy,
            visibleCreate: false
          }, () => {
            this[this.state.apiName](body)
          })
        }
      })
    )
	}

  handleExport = () => {
    trackPromise(
      services.exportData(this.props.token).then(res => {
        this.handleBlob(res.data)
      })
    )
  }

  handleBlob = data => {
    // download data list user
    var blob = new Blob([data], {type: 'application/vnd.ms-excel'});
    var downloadUrl = URL.createObjectURL(blob);
    var a = document.createElement("a");
    a.href = downloadUrl;
    a.download = "datauser.xls";
    document.body.appendChild(a);
    a.click();
  }

	render() {

		const {page, dataProfil, apiName } = this.state

    const text = 'Apakah Anda yakin akan menghapus data ini?'

    const columnsTable = [
      {
        title: 'Nama User',
        dataIndex: 'nama',
        key: 'nama',
        sorter: true,
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        sorter: true,
      },
      {
        title: 'No Telp',
        dataIndex: 'noTelp',
        key: 'no_telp',
        sorter: true,
      },
      {
        title: 'Tanggal Lahir',
        dataIndex: 'tanggalLahir',
        key: 'tanggal_lahir',
        sorter: true,
      },
      {
        title: 'Aksi',
        dataIndex: '',
        align: 'center',
        key: '',
        render: (data) => <>
          {/* <Button 
            onClick={() => this.handleRedirectDetail(data.idUser)}
            type='link'
            style={{ color: '#00C3D4', cursor: 'pointer', fontWeight: 600 }}
          >
            
            <EyeFilled style={{fontSize: 20}}/>
          </Button> */}
          <Button
            onClick={() => this.handleRedirectEdit(data.email)}
            type='link'
            style={{ color: '#00C3D4', cursor: 'pointer', fontWeight: 600 }}
          >
            <EditOutlined style={{fontSize: 20}} />
          </Button>
          <Popconfirm
            placement="bottomRight"
            title={text}
            onConfirm={() => this.handleReject(data.email)}
            okText="Yes"
            cancelText="No"
          >
            <Button
              type='link'
              style={{ color: '#00C3D4', cursor: 'pointer', fontWeight: 600 }}
            >
              <DeleteOutlined style={{fontSize: 20}} />
            </Button>
          </Popconfirm>
        </>
      },
    ]

		return(
			<div className='root'>
				<Card className='bodyCard_style'>
					<Row gutter={this.props.isMobile ? [16,16] : [16,0]} justify="space-between">
            <Col span={8} xs={24} md={8}>
              <UISearch placeholder='Nama Acara, narasumber...' handleSearch={key => this.handleSearch(key)} />
            </Col>
            <Col span={16} xs={24} md={16} style={{textAlign: !this.props.isMobile & 'end'}}>
              <Button onClick={()=> this.handleExport()} style={{marginRight: '10px'}} className='addButton'>Export</Button>
              <Button onClick={() => this.handleRedirectAdd()} className='addButton'>+ Tambah User</Button>
            </Col>
          </Row>
					<Card 
            bordered={false}
            style={{minHeight: '300px'}}>
            <UItableNew
              scroll={{x: 800}}
              apiName={apiName}
              columns={columnsTable}
              token={this.props.token}
              withRows={false}
              page={page}
              data={dataProfil.data}
              totalData={dataProfil.totalData}
              sortDefault=''
            />
          </Card>
				</Card>
			</div>
		)
	}
}


export default User