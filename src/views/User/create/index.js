import React, {Component} from 'react'
import '../../../global/global.css'
import { services } from '../../../api/user'
import { trackPromise } from 'react-promise-tracker';
import {
  Spin,
  Typography,
} from 'antd'
import { UIFormUser } from './component';

const { Text } = Typography
class UserCreate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lovJenisPekerjaan: [],
      loading: false,
      isEdit: false,
      initialData: {},
      eventId: null
    }
  }

  componentDidMount() {
    const id = this.props.location.pathname.split('/')[2]
    const type = this.props.location.pathname.split('/')[1]
    if(type == 'user-edit' && id){
      const body = {
        token: this.props.token,
        id: id
      }
      this.setState({
        eventId: id,
        loading: true,
        isEdit: true
      }, () => this.getDataId(body))
    }
  }

  getDataId = param => {
    // get data user berdasarkan yang dipilih oleh user
		trackPromise(
      services.getDataDetail(param).then(res => {
        let dataTemp = {
          ...res.data.data
        }
        this.setState({
          initialData: dataTemp,
          loading: false
        })
      })
    )
  }

  editHeaderFooter = (data) => {
    let body = {
      ...data,
      usersId: this.props.userId
    }
    if(this.state.isEdit){
      body.eventId = this.state.eventId
      body.banner = 
      // edit data yang telah di rubah oleh user
      trackPromise(
        services.edit(this.props.token, this.state.eventId, body).then((res) => {
          if(res == 'success'){
            this.props.history.push('/user')
          }
        })
      )
    }else{
      // membuat data baru berdasarkan data yang telah di inputkan oleh user
      trackPromise(
        services.create(this.props.token, body).then((res) => {
          if(res == 'success'){
            this.props.history.push('/user')
          }
          return true
        })
      )
    }
  }

  render(){
    const { lovJenisPekerjaan, loading, isEdit, initialData } = this.state
    return (
      <div className='root'>
        <Spin spinning={loading}>
          <UIFormUser 
            data={lovJenisPekerjaan}
            onFinish={(e)=>this.editHeaderFooter(e)}
            getLov={e => this.getData(e)}
            isMobile={this.props.isMobile}
            token = {this.props.token}
            isEdit = {isEdit}
            initialData = {initialData}
          />
        </Spin>
      </div>
    )
  }
}


export default UserCreate
