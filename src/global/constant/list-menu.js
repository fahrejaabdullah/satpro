import { ConsoleSqlOutlined } from "@ant-design/icons";

const listMenu = {
  'Admin': [
    {
      label: 'Dashboard',
      path: 'dashboard',
    },
    {
      label: 'Katalog Acara',
      path: 'katalog-acara-admin'
    },
    {
      label: 'Katalog Acara',
      path: 'katalog-detail-admin',
      isHidden: true
    },
    {
      label: 'Hadiah / Doorprize',
      path: 'hadiah-doorprize',
    },
    {
      label: 'User',
      path: 'user'
    },
    {
      label: 'Pembayaran',
      path: 'pembayaran',
    },
    // {
    //   label: 'Yayasan',
    //   children: [
    //     {
    //       label: 'Galeri',
    //       path: 'galeri',
    //     },
    //     {
    //       label: 'Detail Galeri',
    //       path: 'galeri-detail',
    //       isHidden: true
    //     },
    //     {
    //       label: 'Tambah Data Galeri',
    //       path: 'galeri-add',
    //       isHidden: true
    //     },
    //     {
    //       label: 'Ubah Data Galeri',
    //       path: 'galeri-edit',
    //       isHidden: true
    //     },
    //     {
    //       label: 'Styled Test',
    //       path: 'testing-styled',
    //     },
    //   ]
    // },
  ],
  'Free Role': [
    {
      label: 'Katalog Acara',
      path: 'katalog-acara',
    },
    // {
    //   label: 'Donor Beasiswa',
    //   path: 'donor-beasiswa',
    // },
    // {
    //   label: 'Donasi Sukarela',
    //   path: 'donasi-sukarela',
    // },
    // {
    //   label: 'Laporan',
    //   path: 'laporan',
    // },
  ],
  'User': [
    {
      label: 'Katalog Acara',
      path: 'katalog-acara',
    },
    {
      label: 'Doorprize',
      path: 'doorprize',
    },
    {
      label: 'Acara Saya',
      path: 'acara-saya',
    },
  ]
};

const getListMenuByRole = (verif) => {
  const currentRole = sessionStorage.getItem('userRole') ?? localStorage.getItem("userRole");
  let tempListMenu = {
    'User': [],
    'Free Role': [],
    'Admin': []
  }
  listMenu[currentRole].map(res => {
    let found = false; 
    for (const e of tempListMenu[currentRole]) {
      if (e.label === res.label) {
        found = true;
        break;
      }
    }
    if(!found) {
      if (res.label === 'Katalog Acara') {
          tempListMenu[currentRole].push({
            label: res.label,
            path: res.path
          })
      }else {
        if (!verif) {
          tempListMenu[currentRole].push({
            label: res.label,
            path: res.path,
            isHidden: true
          })
        }else {
          tempListMenu[currentRole].push({
            label: res.label,
            path: res.path
          })
        }
      }
    }
  })
  return tempListMenu[currentRole] || [];
}

export {listMenu, getListMenuByRole};
