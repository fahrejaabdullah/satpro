const dotenv = require('dotenv');

dotenv.config();

const env = process.env.NODE_ENV


const options = {
	development: {
		BASE_URL: "https://apis.elqurantv.com/",
		COPYRIGHT: '20200716',
		VERSION: '0.0.2',
		TIMEOUT: 30000,
	},
	production: {
		BASE_URL: "https://apis.elqurantv.com/",
		COPYRIGHT: '20200716',
		VERSION: '0.0.2',
		TIMEOUT: 30000,
	},
}
const config = options[env]

export default {
	...config
}


