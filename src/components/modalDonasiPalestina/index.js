import React from 'react'
import {Col, Modal,Row} from 'antd';
import './index.scss';
import urlLogo from '../../global/assets/logo.png'
import urlBsi from '../../global/assets/bsi.png'
import urlBni from '../../global/assets/bni.png'

export default function ModalDonasiPalestina(props) {
  const {visible, onCancel,} = props;
  return (
    <Modal
      className="app-modal"
      width={400}
      footer={null}
      visible={visible}
      onCancel={onCancel}
    >
        <Row gutter={14} justify="center">
            <Col span={24} style={{textAlign:"center"}}>
                <img className="imgHeaderModal" src={urlLogo}/>
            </Col>
            <Col span={24} style={{fontSize:24,color:"#00C3D4",textAlign:"center",fontWeight:700,marginTop:10}}>
                Donasi Untuk Palestina
            </Col>
            <Col span={24} style={{fontSize:14,color:"#7D7987",textAlign:"center",fontWeight:400}}>
                Untuk donasi, sedekah, juga infaq kepada saudara kita. <br/> Kami hanya menerima melalui rekening 
            </Col>
            <Col span={24} style={{marginTop:20}}>
                <Row>
                    <Col span={12} style={{textAlign:"end"}}>
                        <img src={urlBsi}/>

                    </Col>
                    <Col span={12}>
                        <img src={urlBni} style={{marginTop:"20px"}} />
                    </Col>
                </Row>
            </Col>
            <Col span={24} style={{fontSize:24,color:"#00C3D4",textAlign:"center",fontWeight:700,marginTop:10}}>
                0992992551
            </Col>
            <Col span={24} style={{fontSize:14,textAlign:"center",marginTop:10}}>
                <Row>
                    <Col span={9} style={{textAlign:"end"}}>
                        Atas Nama : 
                    </Col>
                    <Col span={15} style={{color:"#00C3D4",fontWeight:700,textAlign:"start"}}>
                        &nbsp;Yayasan Bina Qur’an
                    </Col>
                </Row>
            </Col>
        </Row>
    </Modal>
  )
}
