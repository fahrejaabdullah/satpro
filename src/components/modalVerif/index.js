import React, { useEffect, useState } from 'react'
import { Modal, Button, Row, Col } from 'antd'
import '../../styles/modal.scss'
import { post } from 'axios'
import config from '../../services/config'
import {CheckCircleOutlined} from '@ant-design/icons'
const { BASE_URL } = config


const ModalVerifCore = props => {
  const [dataUser, setDataUser] = useState(props?.dataUser || {
    isVerif: false,
    nama: '',
    email: ''
  })
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    setDataUser(props.dataUser)
    setLoading(false)
  }, [props.dataUser])

  const onCancel = () => {
    setDataUser({
      ...dataUser,
      isVerif: false
    })
  }

  const verifEmail = () => {
    setLoading(true)
    post(`${BASE_URL}rest/web/open/auth/kirim-verikasi`,{ email: dataUser.email}, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(res => {
      sessionStorage.clear();
      localStorage.clear();
      setLoading(false);
      props.dataUser.history.push('/')
    })
    .catch(error => {
      setLoading(false);
    })
  }
  
  return(
    <Modal 
      className="app-modal"
      visible={dataUser.isVerif}
      onCancel={_ => onCancel()}
      footer={null}
      centered
    >
      <Row justify="center">
        <Col span={24} style={{textAlign: 'center', marginBottom: '20px'}} >
          <CheckCircleOutlined 
            style={{color: '#00C3D4', fontSize: '56px'}}

          />
        </Col>
        <Col span={24} style={{textAlign: 'center'}}>
          <span className='verifText'>
            Verifikasi email kamu
          </span>
        </Col>
        <Col span={24} style={{textAlign: 'center', marginBottom: '30px'}}>
          <span className='verifTextDetail'>
            Hi {dataUser.nama} ! klik tombol dibawah ini untuk memverifikasi akun dengan email kamu
          </span>
        </Col>
        <Col span={16}>
          <Button 
            loading={loading}
            className='addButton' 
            style={{
              width: '100%', 
              width: '-webkit-fill-available', 
              borderRadius: '55px'
              }}
            onClick={() => verifEmail()}
          >
            Verifikasi Email
          </Button>
        </Col>
      </Row>
    </Modal>
  )
}

export const UIModalVerif = ModalVerifCore

