import React, { useState, useEffect } from 'react'
import { Table, Row, Col } from 'antd'
import { usePromiseTracker, trackPromise } from "react-promise-tracker";
import { endpoint } from '../../api/apiDataTable'

const NewTableCore = (props) => {

  const [dafaultRow, setDefaultRow] = useState(props.rowSelect)

  const { promiseInProgress } = usePromiseTracker();

	let [dataTable, setDataTable] = useState({
		data: props.data,
		totalPage: props.totalData
	})
	const [param, setParam] = useState({
		token: props.token,
		sort: props.defaultOrder,
		size: 10,
		page: 1,
		search: '',
	})
  const [paramExtend, setExtendParam] = useState({
    token: props.token,
		sort: props.defaultOrder,
		size: 10,
		page: 1,
		search: '',
    ...props.extendParam
  })

  useEffect(() => {
    setDefaultRow(props.rowSelect);
  }, [props.rowSelect]);

  useEffect(() => {
    setDataTable({
      data: props.data,
      totalPage: props.totalData
    })
  }, [props.data])

	useEffect(() => {
    if (props.isExtendParam) {
      getData(paramExtend)
    }else{
      getData(param)
    }
	}, [param, paramExtend])

	const getData = (body) => {
		trackPromise(
			endpoint[props.apiName](body).then(res=> { // get naming function dynamic
				setDataTable(res)
			})
		)
	}

  const defaultColums = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id'
    },
    {
      title: 'name',
      dataIndex: 'name',
      key: 'name'
    },
  ]

  const handleTableChange = (page, filters, sorter) => {
    if (sorter.order === "ascend") {
      sorter.order = "asc";
    } else if (sorter.order === "descend") {
        sorter.order = "desc";
    }
    const sortingPage = sorter.order ? `${sorter.columnKey},${sorter.order}` : props.sortDefault
    if (props.isExtendParam) {
      setExtendParam({
        ...paramExtend,
        sort: sortingPage,
        page: page.current
      })
    }else {
      setParam({
        ...param,
        sort: sortingPage,
        page: page.current
      })
    }
  }

  const onSelectChange = selectedRowKeys => {
    props.checkedBox(selectedRowKeys)
    setDefaultRow(selectedRowKeys)
  };

  let pagination = {
    defaultCurrent: 1,
    current: param.page,
    pageSize: 10,
    showSizeChanger: false,
    size: 'small',
    total: dataTable.totalData
  }
  
  const columns = props.columns || defaultColums

  const rowSelection = {
    selectedRowKeys: dafaultRow,
    onChange: onSelectChange,
    hideSelectAll: true
  };

  const withRows = props.withRows === false ? props.withRows : true

  return(
    <Row>
      <Col span={24}>
      {withRows ? (
        <Table
          size={props.size}
          dataSource={dataTable.data}
          columns={columns}
          loading={promiseInProgress}
          pagination={pagination}
          rowSelection={rowSelection}
          rowKey={record => record.id}
          onChange={(page, filters, sorter) => handleTableChange(page, filters, sorter)}
          scroll={props.scroll}
        />
      ) : (
        <Table
          size={props.size}
          dataSource={dataTable.data}
          columns={columns}
          loading={promiseInProgress}
          pagination={pagination}
          rowKey={record => record.id}
          onChange={(page, filters, sorter) => handleTableChange(page, filters, sorter)}
          scroll={props.scroll}
        />
      )}
      </Col>
    </Row>
  )
}

export const UItableNew = NewTableCore