import React from 'react';

export function RedStar(props) {
  return <span style={{color: 'red'}} {...props}>*</span>
}

