import React from 'react';
import { Row, Col, Upload, Button, Tooltip } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

import config from '../../services/config'
const { BASE_URL } = config

function UploadComponentValues({accept = '', ...props}) {
  return (
    <>
      <Row key={props.key}>
        <Col span={24}>
          <Upload
            action={BASE_URL + props.urlAPI}
            listType="picture"
            defaultFileList={props.defaultBanner}
            maxCount={1}
            onChange={props.onChange}
            su
            headers={{
              Authorization: `Bearer ${props.token}`,
            }}
            accept={props.accept}
            onRemove={props.removeImage}
          >
            <Tooltip placement="top" title="Format jpg, png, jpeg. 500x500px. Maximum 5 MB." arrowPointAtCenter>
              <Button icon={<UploadOutlined />}>{props.placeholder}</Button>
            </Tooltip>
          </Upload>
        </Col>
      </Row>
    </>
  );
}

export default UploadComponentValues;
