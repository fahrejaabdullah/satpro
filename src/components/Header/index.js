import React, { useState,useEffect } from "react";
import { Breadcrumb, Button, Card, Col, Dropdown, Menu, Row } from "antd";
import { DownOutlined } from '@ant-design/icons';
import { withRouter, useHistory } from "react-router-dom";
import { getListMenuByRole } from "../../global/constant/list-menu";
import './index.css'
import ModalUbahPassword from "../modalUbahPassword";

const findPath = (path, obj) => {
  const route = [];
  obj.forEach(e => {
    if (e.path && e.path === path) {
      route[0] = e.label;
    } else if (e.children) {
      e.children.forEach(ec => {
        if (ec.path && ec.path === path) {
          route[0] = e.label;
          route[1] = ec.label;
        }
      });
    }
  });
  return route
}

const HeaderComponent = (props) => {
  const history = useHistory()
  const [showModal, setShowModal] = useState(false);
  const currentPath = (props.location.pathname || '/').split('/')[1];
  
  const userData = sessionStorage.getItem('userData') ? sessionStorage.getItem('userData') : localStorage.getItem('userData') ? localStorage.getItem('userData') : null
  const jsonData = JSON.parse(userData)
  const listMenu = getListMenuByRole(jsonData?.verified === 1 ? true : false);
  const paths = findPath(currentPath, listMenu) || [];
  const currentRole = sessionStorage.getItem('userRole') ?? localStorage.getItem("userRole");
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    if (currentRole !== 'Free Role') {
      var _userData = JSON.parse(userData);
      setIsValid(_userData.verified === 1);
    }
  },[userData]);
  const menu = () => (
    <Menu>
      <Menu.Item onClick={toggleModal}>Ubah Password</Menu.Item>
      <Menu.Item onClick={handleUpdateProfile}>Update Profile</Menu.Item>
      <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
    </Menu>
  )

  const handleLogout = () => {
    sessionStorage.clear();
    localStorage.clear();
    props.history.push("/")
  }

  const handleUpdateProfile = () => {
    var userId= sessionStorage.getItem('userId') ? sessionStorage.getItem('userId') : localStorage.getItem('userId') ? localStorage.getItem('userId') : null;
    if(currentRole === "User"){
      props.history.push("/update-profile");
    }else{
      props.history.push("/update-profile-admin/"+userId);
    }
  }

  const onChangePass = () => {
    toggleModal();
    handleLogout();
  }

  const toggleModal = () => {
    setShowModal(!showModal);
  }

  return (
    <div className={currentRole ? 'headerAdmin' : 'header'}>
      <Row justify="center">
        <Col span={24}>
          <Card className="app-card">
            <Row gutter={props.isMobile && [0, 8]} justify="space-between">
              {props.isMobile && (
                props.name ? (
                  <Col span={props.isMobile && 24}>
                    <Col span={24}>
                      <Dropdown overlay={menu} placement="bottomCenter" arrow trigger={['click']}>
                        <Col span={24} style={{ cursor: 'pointer' }}>
                          <span>Hi, </span>
                          <span className="bold_text" style={{marginRight: '20px'}}>{props.name} <DownOutlined /></span>
                        </Col>
                      </Dropdown>
                    </Col>
                    {
                      !isValid ? (
                      <Col span={24}>
                        <div style={{marginRight:"20px", fontWeight:700,textAlign:"end"}}>
                          User belum terverifikasi
                        </div>
                      </Col>
                      ):<></>
                    }
                  </Col>
                ) : (
                  <Col span={props.isMobile && 24}>
                    <Button className='loginButton' onClick={() => history.push(`/login`)}>
                      Login
                    </Button>
                  </Col>
                )
              )}
              <Col span={props.isMobile && 24} style={{ flex: 1, textAlign: 'left' }}>
                {currentRole !== 'Free Role' ? (
                  <Breadcrumb className='breadcrumb_style'>
                    <Breadcrumb.Item >{currentRole}</Breadcrumb.Item>
                    {paths.map((breadCrumb, i) => {
                      return (<Breadcrumb.Item className='breadcrum_item' key={`breadCrumb-${i}`}>{breadCrumb}</Breadcrumb.Item>)
                    })}
                  </Breadcrumb>
                ) : (
                  paths.map((breadCrumb, i) => {
                    return (<Breadcrumb.Item key={`breadCrumb-${i}`}>{breadCrumb}</Breadcrumb.Item>)
                  })
                )}
              </Col>
              {!props.isMobile && (
                props.name ? (
                  <Col span={props.isMobile && 24}>
                    <Col span={24}>
                      <Dropdown overlay={menu} placement="bottomCenter" arrow trigger={['click']}>
                        <Col span={24} style={{ cursor: 'pointer' }}>
                          <span>Hi, </span>
                          <span className="bold_text" style={{marginRight: '20px'}}>{props.name} <DownOutlined /></span>
                        </Col>
                      </Dropdown>
                    </Col>
                    {
                      !isValid ? (
                      <Col span={24}>
                        <div style={{marginRight:"20px", fontWeight:700,textAlign:"end"}}>
                          User belum terverifikasi
                        </div>
                      </Col>
                      ):<></>
                    }
                  </Col>
                ) : (
                  <Button className='loginButton' onClick={() => history.push(`/login`)}>
                    Login
                  </Button>
                )
              )}
            </Row>
          </Card>
        </Col>
      </Row>
      <ModalUbahPassword visible={showModal} onCancel={toggleModal} onFinish={onChangePass} />
    </div>
  )
};

export default withRouter(HeaderComponent);
