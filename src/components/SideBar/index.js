import React, { useEffect, useState, } from "react";
import { Layout, Menu, Typography } from 'antd'
import { withRouter, NavLink,Link,useHistory } from "react-router-dom";
import { getListMenuByRole } from '../../global/constant/list-menu';
import './sidebar.scss';
import { CalendarOutlined, PieChartOutlined, UserAddOutlined, WalletOutlined, GiftOutlined, ScheduleOutlined} from "@ant-design/icons";

const { Sider, Header } = Layout;
const { SubMenu } = Menu;
const { Text } = Typography;
const iconMenu = {
  Dashboard: { icon: <PieChartOutlined/>},
  Pengaturan: { icon: <UserAddOutlined/>},
  Pembayaran: { icon: <WalletOutlined />},
  Doorprize: { icon: <GiftOutlined />},
  Acara: { icon: <ScheduleOutlined />}
}

const SideBarComponent = (props) => {
  const [collapsed, setCollapsed] = useState(false);
  const userData = sessionStorage.getItem('userData') ? sessionStorage.getItem('userData') : localStorage.getItem('userData') ? localStorage.getItem('userData') : null
  const jsonData = JSON.parse(userData)
  const listMenu = getListMenuByRole(jsonData?.verified === 1 ? true : false);
  const currentRole = sessionStorage.getItem('userRole') ? sessionStorage.getItem('userRole') : localStorage.getItem('userRole') ? localStorage.getItem('userRole') : "Free Role";
  const history = useHistory();

  const onCollapse = val => {
    setCollapsed(val)
  }

  const handleRoute = () => {
    if(currentRole === "User" || currentRole === "Free Role"){
      history.push("/");
    }else{
      // do nothing
    }
  }

  useEffect(() => {
    if(window.innerWidth <= 600 && !collapsed){
      setCollapsed(true)
    }
  },[]);

  const changeMenu = (e) => {
    if(window.innerWidth <= 600 && !collapsed){
      setCollapsed(true)
    }
    return true
  }

  return (
    props.isMobile ? (
      <>  
        <div className="logoMobile">
          <Link onClick={()=>handleRoute()}><div className={"logo-collapsed"} /></Link>
        </div>
          <Menu theme="light" defaultSelectedKeys={props.location.pathname} mode="horizontal" style={{background: '#DFE0E1' }} onClick={e=> changeMenu(e)}>
            {/* Generate menu */}
            {listMenu.map((menu, i) => {
              const hasChildren = menu.children && menu.children.length > 0;
              const key = `navItem-${i}`;
              const menuName = menu.label.split(' ')
              const iconName = menuName[0]
              return !hasChildren ? (
                !menu.isHidden &&
                <Menu.Item key={menu.path}  title={menu.label} icon={iconMenu[iconName] ? iconMenu[iconName].icon : <CalendarOutlined/>}>
                  <NavLink to={`/${menu.path}`}>{menu.label}</NavLink>
                </Menu.Item>
              ) : (
                <SubMenu key={key} title={menu.label} icon={iconMenu[iconName] ? iconMenu[iconName].icon : <PieChartOutlined/>}>
                  {menu.children.map((submenu, si) => {
                    return !submenu.isHidden ? (
                      <Menu.Item key={submenu.path} title={submenu.label}>
                        <NavLink to={`/${submenu.path}`}>{submenu.label}</NavLink>
                      </Menu.Item>
                    ) : (<></>);
                  })}
                </SubMenu>
              );
            })}
          </Menu>
      </>
    ) : (
      <Sider theme="light" width={200} collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <Link onClick={()=>handleRoute()}><div className={collapsed ? "logo-collapsed" : "logo"} /></Link>
        <Menu theme="light" defaultSelectedKeys={props.location.pathname} mode="inline" style={{background: '#DFE0E1' }} onClick={e=> changeMenu(e)}>
          {/* Generate menu */}
          {listMenu.map((menu, i) => {
            const hasChildren = menu.children && menu.children.length > 0;
            const key = `navItem-${i}`;
            const menuName = menu.label.split(' ')
            const iconName = menuName[0]
            return !hasChildren ? (
              !menu.isHidden &&
              <Menu.Item key={menu.path}  title={menu.label} icon={iconMenu[iconName] ? iconMenu[iconName].icon : <CalendarOutlined/>} style={{paddingRight: 0}}>
                <NavLink to={`/${menu.path}`}>{ collapsed ? '' : menu.label}</NavLink>
              </Menu.Item>
            ) : (
              <SubMenu key={key} title={menu.label} icon={iconMenu[iconName] ? iconMenu[iconName].icon : <PieChartOutlined/>}>
                {menu.children.map((submenu, si) => {
                  return !submenu.isHidden ? (
                    <Menu.Item key={submenu.path} title={submenu.label}>
                      <NavLink to={`/${submenu.path}`}>{submenu.label}</NavLink>
                    </Menu.Item>
                  ) : (<></>);
                })}
              </SubMenu>
            );
          })}
        </Menu>
      </Sider>
    )
  )
};

export default withRouter(SideBarComponent);
